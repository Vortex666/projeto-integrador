﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    class ConsoleComputadorDatabase
    {
        public int Salvar(ConsoleComputadorDTO consoleComputador)
        {
            string script =
                @"INSERT INTO Console_Computador (Nm_Cons_Comp, Ds_Marca, Ds_Pecas_Adicionais, Ds_Configuracoes, @Qt_Cons_Pc, Vl_Preco_Total, Vl_Preco)
                    VALUE (@Nm_Cons_Comp, @Ds_Marca, @Ds_Pecas_Adicionais, @Ds_Configuracoes, @Qt_Cons_Pc, @Vl_Preco_Total, @Vl_Preco_Pecas)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_Cons_Comp", consoleComputador.Nome));
            parms.Add(new MySqlParameter("Ds_Marca", consoleComputador.Marca));
            parms.Add(new MySqlParameter("Ds_Pecas_Adicionais", consoleComputador.PecasAdicionais));
            parms.Add(new MySqlParameter("Ds_Configuracoes", consoleComputador.Configuracoes));
            parms.Add(new MySqlParameter("Qt_Cons_Pc", consoleComputador.Quantidade));
            parms.Add(new MySqlParameter("Vl_Preco_Total", consoleComputador.PrecoTotal));
            parms.Add(new MySqlParameter("Vl_Preco", consoleComputador.PrecoPcConsole));



            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<ConsoleComputadorDTO> Listar()
        {
            string script =
                @"SELECT * FROM Console_Computador";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<ConsoleComputadorDTO> consolepc = new List<ConsoleComputadorDTO>();

            while (reader.Read())
            {
                ConsoleComputadorDTO dto = new ConsoleComputadorDTO();
                dto.Id = reader.GetInt32("Id_Cons_Comp");
                dto.Nome = reader.GetString("Nm_Cons_Comp");
                dto.Marca = reader.GetString("Ds_Marca");
                dto.PecasAdicionais = reader.GetString("Ds_Pecas_Adicionais");
                dto.Configuracoes = reader.GetString("Ds_Configuracoes");
                dto.Quantidade = reader.GetInt32("Qt_Cons_Pc");
                dto.PrecoTotal = reader.GetInt32("Vl_Preco_Total");
                dto.PrecoPcConsole = reader.GetInt32("Vl_Preco");



                consolepc.Add(dto);

            }

            return consolepc;

        }

        public void Alterar(ConsoleComputadorDTO  conspc)
        {

            string script = @"UPDATE Console_Computador SET 
                                                 Nm_Cons_Comp = @Nm_Cons_Comp,
                                                 Ds_Marca = @Ds_Marca, 
                                                 Ds_Pecas_Adicionais = @Ds_Pecas_Adicionais,
                                                 Ds_Configuracoes = @Ds_Configuracoes,
                                                 Qt_Cons_Pc = @Qt_Cons_Pc,
                                                 Vl_Preco_Total = @Vl_Preco_Total, 
                                                 Vl_Preco = @Vl_Preco,

                             WHERE Id_Cons_Comp = @Id_Cons_Comp;";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_Cons_Comp", conspc.Nome));
            parms.Add(new MySqlParameter("Ds_Marca", conspc.Marca));
            parms.Add(new MySqlParameter("Ds_Pecas_Adicionaisa", conspc.PecasAdicionais));
            parms.Add(new MySqlParameter("Ds_Configuracoes", conspc.Configuracoes));
            parms.Add(new MySqlParameter("Qt_Cons_Pc", conspc.Quantidade));
            parms.Add(new MySqlParameter("Vl_Preco_Total", conspc.PrecoTotal));
            parms.Add(new MySqlParameter("Vl_Preco", conspc.PrecoPcConsole));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
    }
}
