﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;


namespace TelasDaADM.DB.Databases
{
    public class FuncionarioDatabase
    {

        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM Funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {

                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("Id_Func");
                funcionario.Nome = reader.GetString("Nm_Func");
                funcionario.DataNascimento = reader.GetDateTime("Dt_Nascimento");
                funcionario.CPF = reader.GetString("Ds_CPF");
                funcionario.RG = reader.GetString("Ds_RG");
                funcionario.Cargo = reader.GetString("Nm_Cargo");
                funcionario.Observacao = reader.GetString("Ds_Observacao");
                funcionario.Login = reader.GetString("Ds_Login");
                funcionario.Senha = reader.GetString("Ds_Senha");  
                funcionario.Email = reader.GetString("Ds_Email");
                funcionario.Telefone = reader.GetString("Ds_Telefone");
                funcionario.Salario = reader.GetDecimal("Vl_Salario");
                funcionario.Admin = reader.GetString("Bt_Admin");
                funcionario.IdEndereco = reader.GetInt32("Endereco_Id_Endereco");



                lista.Add(funcionario);
            }
            reader.Close();

            return lista;
        }


        public int Salvar(FuncionarioDTO Funcionario)
        {
            string Script = @"INSERT INTO Funcionario
                            (
                                    Nm_Func,
                                    Dt_Nascimento,
                                    Ds_CPF,
                                    Ds_RG,
                                    Nm_Cargo,
                                    Ds_Observacao,
                                    Ds_Login,
                                    Ds_Senha,
                                   
                                    Ds_Email,
                                    Ds_Telefone,
                                    Vl_Salario,
                                    Bt_Admin,
                                    Endereco_Id_Endereco
                                    
                                                               
                                    
                            )
                            VALUES
                            (       
                                    @Nm_Func,
                                    @Dt_Nascimento,
                                    @Ds_CPF,
                                    @Ds_RG,
                                    @Nm_Cargo,
                                    @Ds_Observacao,
                                    @Ds_Login,
                                    @Ds_Senha,
                                                         
                                    @Ds_Email,
                                    @Ds_Telefone,
                                    @Vl_Salario,
                                    @Bt_Admin,
                                    @Endereco_Id_Endereco
                                    
                                    
                             );";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_Func", Funcionario.Nome));
            parms.Add(new MySqlParameter("Dt_Nascimento", Funcionario.DataNascimento));
            parms.Add(new MySqlParameter("Ds_CPF", Funcionario.CPF));
            parms.Add(new MySqlParameter("Ds_RG", Funcionario.RG));
            parms.Add(new MySqlParameter("Nm_Cargo", Funcionario.Cargo));
            parms.Add(new MySqlParameter("Ds_Observacao", Funcionario.Observacao));
            parms.Add(new MySqlParameter("Ds_Login", Funcionario.Login));
            parms.Add(new MySqlParameter("Ds_Senha", Funcionario.Senha));
            
            parms.Add(new MySqlParameter("Ds_Email", Funcionario.Email));
            parms.Add(new MySqlParameter("Ds_Telefone", Funcionario.Telefone));
            parms.Add(new MySqlParameter("Vl_Salario", Funcionario.Salario));
            parms.Add(new MySqlParameter("Bt_Admin", Funcionario.Admin));
            parms.Add(new MySqlParameter("Endereco_Id_Endereco", Funcionario.IdEndereco));
           

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(Script, parms);
            return pk;


        }
        public void Alterar(FuncionarioDTO Funcionario)
        {
            string script =
            @"UPDATE TB_FUNCIONARIO
                 SET                Nm_Func           = @Nm_Func,
                                    Dt_Nascimento     = @Dt_Nascimento,
                                    Ds_CPF            = @Ds_CPF,
                                    Ds_RG             = @Ds_RG,
                                    Nm_Cargo          = @Nm_Cargo,
                                    Ds_Observacao     = @Ds_Observacao,
                                    Ds_Login          = @Ds_Login, 
                                    Ds_Senha          = @Ds_Senha,        
                                      
                                    Ds_Email          = @Ds_Email,   
                                    Ds_Telefone       = @Ds_Telefone,
                                    Vl_Salario        = @Vl_Salario,
                                    Bt_Admin          = @Bt_Admin,    
                                    Endereco_Id_Endereco = @Endereco_Id_Endereco
                                   
                                    
               WHERE Id_Func = @Id_Func";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_Func", Funcionario.Nome));
            parms.Add(new MySqlParameter("Dt_Nascimento", Funcionario.DataNascimento));
            parms.Add(new MySqlParameter("Ds_CPF", Funcionario.CPF));
            parms.Add(new MySqlParameter("Ds_RG", Funcionario.RG));
            parms.Add(new MySqlParameter("Nm_Cargo", Funcionario.Cargo));
            parms.Add(new MySqlParameter("Ds_Observacao", Funcionario.Observacao));
            parms.Add(new MySqlParameter("Ds_Login", Funcionario.Login));
            parms.Add(new MySqlParameter("Ds_Senha", Funcionario.Senha));
            
            parms.Add(new MySqlParameter("Ds_Email", Funcionario.Email));
            parms.Add(new MySqlParameter("Ds_Telefone", Funcionario.Telefone));
            parms.Add(new MySqlParameter("Vl_Salario", Funcionario.Salario));
            parms.Add(new MySqlParameter("Bt_Admin", Funcionario.Admin));
            parms.Add(new MySqlParameter("Endereco_Id_Endereco", Funcionario.IdEndereco));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int idFunc)
        {
            string script =
            @"DELETE FROM Funcionario WHERE Id_Func = @Id_Func";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Id_Funcionario", idFunc));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<FuncionarioDTO> Consultar(string nome)
        {
            string script =
            @"SELECT * 
                FROM Funcionario
                WHERE Nm_Func LIKE @Nm_Func;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_func", "%" + nome + "%"));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> funcionarios = new List<FuncionarioDTO>();

            while (reader.Read())
            {
                FuncionarioDTO novofuncionario = new FuncionarioDTO();
                novofuncionario.Id = reader.GetInt32("Id_Func");
                novofuncionario.Nome = reader.GetString("Nm_Func");
                novofuncionario.DataNascimento = reader.GetDateTime("Dt_Nascimento");
                novofuncionario.CPF = reader.GetString("Ds_CPF");
                novofuncionario.RG = reader.GetString("Ds_RG");
                novofuncionario.Cargo = reader.GetString("Nm_Cargo");
                novofuncionario.Observacao = reader.GetString("Ds_Observacao");
                novofuncionario.Login = reader.GetString("Ds_Login");
                novofuncionario.Senha = reader.GetString("Ds_Senha");
               
                novofuncionario.Email = reader.GetString("Ds_Email");
                novofuncionario.Telefone = reader.GetString("Ds_Telefone");
                novofuncionario.Salario = reader.GetDecimal("Vl_Salario");
                novofuncionario.Admin = reader.GetString("Bt_Admin");
                novofuncionario.IdEndereco = reader.GetInt32("Endereco_Id_Endereco");


                funcionarios.Add(novofuncionario);
            }
            reader.Close();

            return funcionarios;

        }
        public FuncionarioDTO Logar(string login, string senha)
        {
            string script = @"SELECT * FROM Funcionario
                               WHERE Ds_Login = @Ds_Login 
                                 AND Ds_Senha = @Ds_Senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Ds_Login", login));
            parms.Add(new MySqlParameter("Ds_Senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;

            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("Id_Func");
                funcionario.Nome = reader.GetString("Nm_Func");
                funcionario.DataNascimento = reader.GetDateTime("Dt_Nascimento");
                funcionario.CPF = reader.GetString("Ds_CPF");
                funcionario.RG = reader.GetString("Ds_RG");
                funcionario.Cargo = reader.GetString("Nm_Cargo");
                funcionario.Observacao = reader.GetString("Ds_Observacao");
                funcionario.Login = reader.GetString("Ds_Login");
                funcionario.Senha = reader.GetString("Ds_Senha");
                
                funcionario.Email = reader.GetString("Ds_Email");
                funcionario.Telefone = reader.GetString("Ds_Telefone");
                funcionario.Salario = reader.GetDecimal("Vl_Salario");
                funcionario.Admin = reader.GetString("Bt_Admin");
                funcionario.IdEndereco = reader.GetInt32("Endereco_Id_Endereco");




            }

            reader.Close();

            return funcionario;
        }

        

            
    }
}
