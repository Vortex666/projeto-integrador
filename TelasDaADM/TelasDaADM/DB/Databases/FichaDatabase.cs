﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class FichaDatabase
    {
        public int Salvar(FichaDTO ficha)
        {
            string script =
                @"INSERT INTO Ficha(Qt_Ficha, Vl_Preco)
                    VALUE (@Qt_Ficha, @Vl_Preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Qt_Ficha", ficha.Quantidade));
            parms.Add(new MySqlParameter("Vl_Preco", ficha.Preço));




            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<FichaDTO> Listar()
        {
            string script =
                @"SELECT * FROM Ficha";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<FichaDTO> ficha = new List<FichaDTO>();

            while (reader.Read())
            {
                FichaDTO dto = new FichaDTO();
                dto.Id = reader.GetInt32("Id_Ficha");
                dto.Quantidade = reader.GetInt32("Qt_Ficha");
                dto.Preço = reader.GetDecimal("Vl_Preco");
                


                ficha.Add(dto);

            }

            reader.Close();

            return ficha;
        }
    }
}
