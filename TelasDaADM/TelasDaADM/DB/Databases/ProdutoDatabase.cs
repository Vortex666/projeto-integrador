﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO produto)
        {
            string script =
                @"INSERT INTO Produto(Nm_Produto, Qt_Produto, Vl_Preco, Ds_Descricao, Nm_Marca, Ds_Modelo, Vl_Desconto)
                    VALUE (@Nm_Produto, @Qt_Produto, @Vl_Preco, @Ds_Descricao, @Nm_Marca, @Ds_Modelo, @Vl_Desconto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_Produto", produto.Nome));
            parms.Add(new MySqlParameter("Qt_Produto", produto.Quantidade));
            parms.Add(new MySqlParameter("Vl_Preco", produto.Preco));
            parms.Add(new MySqlParameter("Ds_Descricao", produto.Descricao));
            parms.Add(new MySqlParameter("Nm_Marca", produto.Marca));
            parms.Add(new MySqlParameter("Ds_Modelo", produto.Modelo));
            parms.Add(new MySqlParameter("Vl_Desconto", produto.Desconto));








            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<ProdutoDTO> Listar()
        {
            string script =
                @"SELECT * FROM Produto";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<ProdutoDTO> produto = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("Id_Produto");
                dto.Nome = reader.GetString("Nm_Produto");
                dto.Quantidade = reader.GetInt32("Qt_Produto");
                dto.Preco = reader.GetDecimal("Vl_Preco");
                dto.Descricao = reader.GetString("Ds_Descricao");
                dto.Marca = reader.GetString("Nm_Marca");
                dto.Modelo = reader.GetString("Ds_Modelo");
                dto.Desconto = reader.GetDecimal("Vl_Desconto");





                produto.Add(dto);

            }

            reader.Close();

            return produto;
        }
    }
}
