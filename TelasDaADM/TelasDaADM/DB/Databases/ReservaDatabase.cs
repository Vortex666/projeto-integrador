﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class ReservaDatabase
    {
        public int Salvar(ReservaDTO reserva)
        {
            string script =
                @"INSERT INTO Reserva(Dt_Reserva, Ficha_Id_Ficha, Cliente_id_Cliente, Periodo_Id_Periodo)
                    VALUE (@Dt_Reserva, @Ficha_Id_Ficha, @Cliente_id_Cliente, @Periodo_Id_Periodo)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Dt_Reserva", reserva.Data));
            parms.Add(new MySqlParameter("Ficha_Id_Ficha", reserva.IdFicha));
            parms.Add(new MySqlParameter("Cliente_id_Cliente", reserva.IdCliente));
            parms.Add(new MySqlParameter("Periodo_Id_Periodo", reserva.IdPeriodo));








            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }


        public List<ReservaDTO> Listar()
        {
            string script =
                @"SELECT * FROM Reserva";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<ReservaDTO> res1 = new List<ReservaDTO>();

            while (reader.Read())
            {
                ReservaDTO dto = new ReservaDTO();
                dto.Id = reader.GetInt32("Id_Reserva");
                dto.Data = reader.GetDateTime("Dt_Reserva");
                dto.IdFicha = reader.GetInt32("Ficha_Id_Ficha");
                dto.IdCliente = reader.GetInt32("Cliente_id_Cliente");
                dto.IdPeriodo = reader.GetInt32("Periodo_Id_Periodo");






                res1.Add(dto);

            }

            reader.Close();

            return res1;
        }
    }
}
