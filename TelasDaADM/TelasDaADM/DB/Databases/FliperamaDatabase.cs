﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class FliperamaDatabase
    {
        public int Salvar(FliperamaDTO flip)
        {
            string script =
                @"INSERT INTO Fliperama(Nm_Fliperama, Nm_Jogo, Qt_Fliperama, Vl_Preco)
                    VALUE (@Nm_Fliperama, @Nm_Jogo, @Qt_Fliperama, @Vl_Preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_Fliperama", flip.Nome));
            parms.Add(new MySqlParameter("Nm_Jogo", flip.Jogo));
            parms.Add(new MySqlParameter("Qt_Fliperama", flip.Quantidade));
            parms.Add(new MySqlParameter("Vl_Preco", flip.Preço));





            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<FliperamaDTO> Listar()
        {
            string script =
                @"SELECT * FROM Fliperama";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<FliperamaDTO> flip = new List<FliperamaDTO>();

            while (reader.Read())
            {
                FliperamaDTO dto = new FliperamaDTO();
                dto.Id = reader.GetInt32("Id_Estoque");
                dto.Nome = reader.GetString("Nm_Fliperama");
                dto.Jogo = reader.GetString("Nm_Jogo");
                dto.Quantidade = reader.GetInt32("Qt_Fliperama");
                dto.Preço = reader.GetDecimal("Vl_Preco");
                



                flip.Add(dto);

            }

            reader.Close();

            return flip;
        }
    }
}
