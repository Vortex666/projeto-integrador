﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class ReservaConsCompFlipDatabase
    {

        public int Salvar(ReservaConsCompFlipDTO reserva)
        {
            string script =
                @"INSERT INTO Reserva_Conso_Comp_Flip(Reserva_Id_Reserva, Console_Computador_Id_Cons_Comp, Fliperama_Id_Flip)
                    VALUE (@Reserva_Id_Reserva, @Console_Computador_Id_Cons_Comp, @Fliperama_Id_Flip)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Reserva_Id_Reserva", reserva.IdReserva));
            parms.Add(new MySqlParameter("Console_Computador_Id_Cons_Comp", reserva.IdConsoleComputador));
            parms.Add(new MySqlParameter("Fliperama_Id_Flip", reserva.IdFliperama));
            







            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<ReservaConsCompFlipDTO> Listar()
        {
            string script =
                @"SELECT * FROM Reserva_Conso_Comp_Flip";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<ReservaConsCompFlipDTO> res = new List<ReservaConsCompFlipDTO>();

            while (reader.Read())
            {
                ReservaConsCompFlipDTO dto = new ReservaConsCompFlipDTO();
                dto.Id = reader.GetInt32("Id_Reser_Perio_Conso_Comp");
                dto.IdReserva = reader.GetInt32("Reserva_Id_Reserva");
                dto.IdConsoleComputador = reader.GetInt32("Console_Computador_Id_Cons_Comp");
                dto.IdFliperama = reader.GetInt32("Fliperama_Id_Flip");
                





                res.Add(dto);

            }

            reader.Close();

            return res;
        }
    }
}
