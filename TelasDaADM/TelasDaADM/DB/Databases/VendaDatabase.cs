﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class VendaDatabase
    {
        public int Salvar(VendaDTO venda)
        {
            string script =
                @"INSERT INTO Venda(Qt_Venda, Dt_Data, Vl_Total_Despesa, Ds_Local, Console_Computador_Id_Cons_Comp, ProdutoVenda_Id_Proc_Venda)
                    VALUE (@Qt_Venda, @Dt_Data, @Vl_Total_Despesa, @Ds_Local, @Console_Computador_Id_Cons_Comp, @ProdutoVenda_Id_Proc_Venda)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Qt_Venda", venda.Quantidade));
            parms.Add(new MySqlParameter("Dt_Data", venda.Data));
            parms.Add(new MySqlParameter("Vl_Total_Despesa", venda.Total));
            parms.Add(new MySqlParameter("Ds_Local", venda.Total));
            parms.Add(new MySqlParameter("Console_Computador_Id_Cons_Comp", venda.IdConsoleComputador));
            parms.Add(new MySqlParameter("ProdutoVenda_Id_Proc_Venda", venda.IdProdutoVenda));








            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }


        public List<VendaDTO> Listar()
        {
            string script =
                @"SELECT * FROM Venda";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<VendaDTO> produto = new List<VendaDTO>();

            while (reader.Read())
            {
                VendaDTO dto = new VendaDTO();
                dto.Id = reader.GetInt32("Id_Venda");
                dto.Quantidade = reader.GetInt32("Qt_Venda");
                dto.Data = reader.GetDateTime("Dt_Data");
                dto.Total = reader.GetDecimal("Vl_Total_Despesa");
                dto.Local = reader.GetString("Ds_Local");
                dto.IdConsoleComputador = reader.GetInt32("Console_Computador_Id_Cons_Comp");
                dto.IdProdutoVenda = reader.GetInt32("ProdutoVenda_Id_Proc_Venda");
                





                produto.Add(dto);

            }

            reader.Close();

            return produto;
        }
    }
}
