﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO estoque)
        {
            string script =
                @"INSERT INTO Estoque(Ds_Local, Qt_Minimo, Qt_Maximo, Qt_Disponivel, ProdutoVenda_Id_Proc_Venda, Console_Computador_Id_Cons_Comp)
                    VALUE (@Ds_Local, @Qt_Minimo, @Qt_Maximo, @Qt_Disponivel, @Produto_Id_Produto, @Console_Computador_Id_Cons_Comp)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Ds_Local", estoque.Local));
            parms.Add(new MySqlParameter("Qt_Minimo", estoque.Minimo));
            parms.Add(new MySqlParameter("Qt_Maximo", estoque.Maximo));
            parms.Add(new MySqlParameter("Qt_Disponivel", estoque.Disponivel));
            parms.Add(new MySqlParameter("ProdutoVenda_Id_Proc_Venda", estoque.IdProdutoVenda));
            parms.Add(new MySqlParameter("Console_Computador_Id_Cons_Comp", estoque.IdConsoleComputador));



            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<EstoqueDTO> Listar()
        {
            string script =
                @"SELECT * FROM Estoque";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<EstoqueDTO> estoque = new List<EstoqueDTO>();

            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.Id = reader.GetInt32("Id_Estoque");
                dto.Local = reader.GetString("Ds_Local");
                dto.Minimo = reader.GetInt32("Qt_Minimo");
                dto.Maximo = reader.GetInt32("Qt_Maxima");
                dto.Disponivel = reader.GetInt32("Qt_Disponivel");
                dto.IdProdutoVenda = reader.GetInt32("ProdutoVenda_Id_Proc_Venda");
                dto.IdConsoleComputador = reader.GetInt32("Console_Computadores_Id_Cons_Comp");



                estoque.Add(dto);

            }

            reader.Close();

            return estoque;
        }
    }
}
