﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class FolhaPagamentoDatabase
    {
        public int Salvar(FolhaPagamentoDTO folha)
        {
            string script =
                @"INSERT INTO Folha_de_Pagamento(Vl_Salario, Vl_VT, Vl_VA, Vl_VR, Vl_Convenio, Vl_FGTS, Vl_INSS, Vl_IRRF 
                Vl_Horas_extra, Vl_Horas_extra_Feriado, Vl_Descontos, Vl_Sindicato, Vl_Bruto, Vl_Liquido, Dt_Valor_Disponivel, Ds_Forma_de_Pagamento,
                Qt_Atrasos, Qt_Faltas, Vl_Salario_Familia, Vl_DSR, Funcionario_Id_Func)
                    VALUE (@Vl_Salario, @Vl_VT, @Vl_VA, @Vl_VR, @Vl_Convenio, @Vl_FGTS, @Vl_INSS, @Vl_IRRF, @Vl_Horas_extra, @Vl_Horas_extra,
                @Vl_Descontos, @Vl_Sindicato, @Vl_Bruto, @Vl_Liquido, @Dt_Valor_Disponivel, @Ds_Forma_de_Pagamento, @Qt_Atrasos,
                @Qt_Atrasos, @Qt_Faltas, @Vl_Salario_Familia, @Vl_DSR, @Funcionario_Id_Func)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Vl_Salario", folha.Salário));
            parms.Add(new MySqlParameter("Vl_VT", folha.ValeTransporte));
            parms.Add(new MySqlParameter("Vl_VA", folha.ValeAlimentação));
            parms.Add(new MySqlParameter("Vl_VR", folha.ValeRefeição));
            parms.Add(new MySqlParameter("Vl_Convenio", folha.Convênio));
            parms.Add(new MySqlParameter("Vl_FGTS", folha.Fgts));
            parms.Add(new MySqlParameter("Vl_INSS", folha.Inss));
            parms.Add(new MySqlParameter("Vl_IRRF", folha.Irrf));
            parms.Add(new MySqlParameter("Vl_Horas_extra", folha.HoraExtra));
            parms.Add(new MySqlParameter("Vl_Horas_extra_Feriado", folha.HoraExtraFeriado));
            parms.Add(new MySqlParameter("Vl_Descontos", folha.HoraExtra));
            parms.Add(new MySqlParameter("Vl_Sindicato", folha.Sindicato));
            parms.Add(new MySqlParameter("Vl_Bruto", folha.SalárioBruto));
            parms.Add(new MySqlParameter("Vl_Liquido", folha.SalárioLíquido));
            parms.Add(new MySqlParameter("Dt_Valor_Disponivel", folha.Disponível));
            parms.Add(new MySqlParameter("Ds_Forma_de_Pagamento", folha.FormaPagamento));
            parms.Add(new MySqlParameter("Qt_Atrasos", folha.Atrasos));
            parms.Add(new MySqlParameter("Qt_Faltas", folha.Faltas));
            parms.Add(new MySqlParameter("Vl_Salario_Familia", folha.SalárioFamilia));
            parms.Add(new MySqlParameter("Vl_DSR", folha.DSR));
            parms.Add(new MySqlParameter("Funcionario_Id_Func", folha.IdFunc));







            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<FolhaPagamentoDTO> Listar()
        {
            string script =
                @"SELECT * FROM Folha_de_Pagamento";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<FolhaPagamentoDTO> folha = new List<FolhaPagamentoDTO>();

            while (reader.Read())
            {
                FolhaPagamentoDTO dto = new FolhaPagamentoDTO();
                dto.Id = reader.GetInt32("Id_Folha_Pagamento");
                dto.Salário = reader.GetDecimal("Vl_Salario");
                dto.ValeTransporte = reader.GetDecimal("Vl_VT");
                dto.ValeAlimentação = reader.GetDecimal("Vl_VA");
                dto.ValeRefeição = reader.GetDecimal("Vl_VR");
                dto.Convênio = reader.GetDecimal("Vl_Convenio");
                dto.Fgts = reader.GetDecimal("Vl_FGTS");
                dto.Inss = reader.GetDecimal("Vl_INSS");
                dto.Irrf = reader.GetDecimal("Vl_IRRF");
                dto.HoraExtra = reader.GetInt32("Vl_Horas_extra");
                dto.HoraExtra = reader.GetInt32("Vl_Horas_extra_Feriado");
                dto.Desconto = reader.GetDecimal("Vl_Descontos");
                dto.Sindicato = reader.GetDecimal("Vl_Sindicato");
                dto.SalárioBruto = reader.GetDecimal("Vl_Bruto");
                dto.Disponível = reader.GetDateTime("Dt_Valor_Disponivel");
                dto.Atrasos = reader.GetInt32("Qt_Atrasos");
                dto.Faltas = reader.GetInt32("Qt_Faltas");
                dto.SalárioFamilia = reader.GetDouble("Vl_Salario_Familia");
                dto.DSR = reader.GetDecimal("Vl_DSR");
                dto.IdFunc = reader.GetInt32("Funcionario_Id_Func");






                folha.Add(dto);

            }

            reader.Close();

            return folha;
        }
    }
}
