﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class VendaFornecedorDatabase
    {

        public int Salvar(VendasFornecedorDTO vendasfornec)
        {
            string script =
                @"INSERT INTO Venda_Fornec(Venda_Id_Venda, Fornecedor_Id_Fornec)
                    VALUE (@Venda_Id_Venda, @Fornecedor_Id_Fornec)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Venda_Id_Venda", vendasfornec.IdVenda));
            parms.Add(new MySqlParameter("Fornecedor_Id_Fornec", vendasfornec.IdFornecedor));
            








            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<VendasFornecedorDTO> Listar()
        {
            string script =
                @"SELECT * FROM Venda_Fornec";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<VendasFornecedorDTO> vendafornec = new List<VendasFornecedorDTO>();

            while (reader.Read())
            {
                VendasFornecedorDTO dto = new VendasFornecedorDTO();
                dto.Id = reader.GetInt32("Id_Venda_Fornec");
                dto.IdVenda = reader.GetInt32("Venda_Id_Venda");
                dto.IdFornecedor = reader.GetInt32("Fornecedor_Id_Fornec");
                






                vendafornec.Add(dto);

            }

            reader.Close();

            return vendafornec;
        }
    }
}
