﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class CompraDatabase
    {
        public int Salvar(CompraDTO compra)
        {
            string script =
                @"INSERT INTO Compra (Vl_Compra, Qt_Compra, Ds_Forma_De_Pagamento, Dt_Compra, Cliente_Id_Cliente)
                    VALUE (@Vl_Compra, @Qt_Compra, @Ds_Forma_De_Pagamento, @Dt_Compra, @Cliente_Id_Cliente)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Vl_Compra", compra.Valor));
            parms.Add(new MySqlParameter("Qt_Compra", compra.Quantidade));
            parms.Add(new MySqlParameter("Ds_Forma_De_Pagamento", compra.FormaDePagamento));
            parms.Add(new MySqlParameter("Dt_Compra", compra.Data));
            parms.Add(new MySqlParameter("Cliente_Id_Cliente", compra.IdCliente));
            

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<CompraDTO> Listar()
        {
            string script =
                @"SELECT * FROM Compra";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<CompraDTO> compra = new List<CompraDTO>();

            while (reader.Read())
            {
                CompraDTO dto = new CompraDTO();
                dto.Id = reader.GetInt32("Id_Compra");
                dto.Valor = reader.GetDecimal("Vl_Compra");
                dto.Quantidade = reader.GetInt32("Qt_Compra");
                dto.FormaDePagamento = reader.GetString("Ds_Forma_De_Pagamento");
                dto.Data = reader.GetDateTime("Dt_Compra");
                dto.IdCliente = reader.GetInt32("Cliente_Id_Cliente");
                

                compra.Add(dto);

            }

            reader.Close();

            return compra;

        }


        public void Alterar(CompraDTO compra)
        {

            string script = @"UPDATE Compra SET 
                                                 Vl_Compra = @Vl_Compra,
                                                 Qt_Compra = @Qt_Compra, 
                                                 Ds_Forma_De_Pagamento = @Ds_Forma_De_Pagamento,
                                                 Dt_Compra = @Dt_Compra,
                                                 Cliente_Id_Cliente =  @Cliente_Id_Cliente
                                                
                                         
                                                
                             WHERE Id_Compra = @Id_Compra;";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Vl_Compra", compra.Valor));
            parms.Add(new MySqlParameter("Qt_Compra", compra.Quantidade));
            parms.Add(new MySqlParameter("Ds_Forma_De_Pagamento", compra.FormaDePagamento));
            parms.Add(new MySqlParameter("Dt_Compra", compra.Data));
            parms.Add(new MySqlParameter("Cliente_Id_Cliente", compra.IdCliente));
            

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
    }
}
