﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class FornecedorDatabase
    {
        public int Salvar(FornecedorDTO fornec)
        {
            string script =
                @"INSERT INTO Fornecedor(Nm_Fornec, Dt_Nascimento, Ds_CNPJ, Ds_RG, Ds_Telefone, Ds_Email, ProdutoVenda_Id_Proc_Venda,
                Console_Computador_Id_Cons_Comp, Endereco_Id_Endereco, Fliperama_Id_Flip)
                    VALUE (@Nm_Fornec, @Ds_CNPJ, @Ds_RG, @Ds_Telefone, @Ds_Email, @ProdutoVenda_Id_Proc_Venda, @Console_Computador_Id_Cons_Comp, @Endereco_Id_Endereco, @Fliperama_Id_Flip)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_Fornec", fornec.Nome));
            parms.Add(new MySqlParameter("Dt_Nascimento", fornec.DataNascimento));
            parms.Add(new MySqlParameter("Ds_CNPJ", fornec.CPFCNPJ));
            parms.Add(new MySqlParameter("Ds_RG", fornec.RG));
            parms.Add(new MySqlParameter("Ds_Telefone", fornec.Telefone));
            parms.Add(new MySqlParameter("Ds_Email", fornec.Email));
            parms.Add(new MySqlParameter("ProdutoVenda_Id_Proc_Venda", fornec.IdProduto));
            parms.Add(new MySqlParameter("Fliperama_Id_Flip", fornec.IdFliperama));
            parms.Add(new MySqlParameter("Console_Computador_Id_Cons_Comp", fornec.IdConsoleComputador));
            parms.Add(new MySqlParameter("Endereco_Id_Endereco", fornec.IdEndereco));





            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<FornecedorDTO> Listar()
        {
            string script =
                @"SELECT * FROM Fornecedor";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<FornecedorDTO> fornec = new List<FornecedorDTO>();

            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id = reader.GetInt32("Id_Fornec");
                dto.Nome = reader.GetString("Nm_Fornec");
                dto.DataNascimento = reader.GetDateTime("Dt_Nascimento");
                dto.CPFCNPJ = reader.GetString("Ds_CNPJ");
                dto.Telefone = reader.GetString("Ds_Telefone");
                dto.RG = reader.GetString("Ds_RG");
                dto.IdFliperama = reader.GetInt32("Fliperama_Id_Flip");
                dto.IdProduto = reader.GetInt32("Produto_Id_Produto");
                dto.IdProduto = reader.GetInt32("Console_COmputador_Id_Cons_Comp");
                dto.IdEndereco = reader.GetInt32("Endereco_Id_Endereco");




                fornec.Add(dto);

            }

            reader.Close();

            return fornec;
        }
    }
}
