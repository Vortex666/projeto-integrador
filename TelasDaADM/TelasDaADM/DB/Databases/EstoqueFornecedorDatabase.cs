﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class EstoqueFornecedorDatabase
    {
        public int Salvar(EstoqueFornecedorDTO estoqueFornecedor)
        {
            string script =
                @"INSERT INTO Estoque_Fornec(Estoque_Id_Estoque, Fornecedor_Id_Fornec)
                    VALUE (@Estoque_Id_Estoque, @Fornecedor_Id_Fornec)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Estoque_Id_Estoque", estoqueFornecedor.IdEstoque));
            parms.Add(new MySqlParameter("Fornecedor_Id_Fornec", estoqueFornecedor.IdFornecedor));




            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<EstoqueFornecedorDTO> Listar()
        {
            string script =
                @"SELECT * FROM Estoque_Fornec";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<EstoqueFornecedorDTO> estoquefornec = new List<EstoqueFornecedorDTO>();

            while (reader.Read())
            {
                EstoqueFornecedorDTO dto = new EstoqueFornecedorDTO();
                dto.Id = reader.GetInt32("Id_Estoque_Fornec");
                dto.IdEstoque = reader.GetInt32("Estoque_Id_Estoque");
                dto.IdFornecedor = reader.GetInt32("Fornecedor_Id_Fornec");
                



                estoquefornec.Add(dto);

            }

            reader.Close();

            return estoquefornec;
        }
    }
}
