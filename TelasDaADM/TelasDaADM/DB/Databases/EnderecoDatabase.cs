﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class EnderecoDatabase
    {
        public int Salvar(EnderecoDTO endereco)
        {
            string script =
                @"INSERT INTO Endereco (Ds_CEP, Nm_Bairro, Nm_Cidade, Ds_Complemento)
                    VALUE (@Ds_CEP, @Nm_Bairro, @Nm_Cidade, @Ds_Complemento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Ds_CEP", endereco.CEP));
            parms.Add(new MySqlParameter("Nm_Bairro", endereco.Bairro));
            parms.Add(new MySqlParameter("Nm_Cidade", endereco.Cidade));
            parms.Add(new MySqlParameter("Ds_Complemento", endereco.Complemento));





            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<EnderecoDTO> Listar()
        {
            string script =
                @"SELECT * FROM Endereco";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<EnderecoDTO> endereco = new List<EnderecoDTO>();

            while (reader.Read())
            {
                EnderecoDTO dto = new EnderecoDTO();
                dto.Id = reader.GetInt32("Id_Endereco");
                dto.CEP = reader.GetString("Ds_CEP");
                dto.Bairro = reader.GetString("Nm_Bairro");
                dto.Cidade = reader.GetString("Nm_Cidade");
                dto.Complemento = reader.GetString("Ds_Complemento");
                




                endereco.Add(dto);

            }

            reader.Close();

            return endereco;
        }

    }
}
