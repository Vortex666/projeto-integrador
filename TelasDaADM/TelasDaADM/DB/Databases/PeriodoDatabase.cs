﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class PeriodoDatabase
    {

        public int Salvar(PeriodoDTO periodo)
        {
            string script =
                @"INSERT INTO Periodo(Nm_Periodo, Dt_Hora_Periodo)
                    VALUE (@Nm_Periodo, @Dt_Hora_Periodo)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_Periodo", periodo.Nome));
            parms.Add(new MySqlParameter("Dt_Hora_Periodo", periodo.Hora));








            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<PeriodoDTO> Listar()
        {
            string script =
                @"SELECT * FROM Periodo";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<PeriodoDTO> periodo = new List<PeriodoDTO>();

            while (reader.Read())
            {
                PeriodoDTO dto = new PeriodoDTO();
                dto.Id = reader.GetInt32("Id_Periodo");
                dto.Nome = reader.GetString("Nm_Periodo");
                dto.Hora = reader.GetDateTime("Dt_Hora_Periodo");
                




               periodo.Add(dto);

            }

            reader.Close();

            return periodo;
        }
    }
}
