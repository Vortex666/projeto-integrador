﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
   public class ClienteDatabase
    {
        public int Salvar(ClienteDTO cliente)
        {
            string script =
                @"INSERT INTO Cliente (Nm_Cliente, Ds_Email, Dt_Nascimento, Ds_Cpf, Ds_Telefone, Ds_RG, Endereco_Id_Endereco)
                    VALUE (@Nm_Cliente, @Ds_Email, @Dt_Nascimento, @Ds_Cpf, @Ds_Telefone, @Ds_RG, @Endereco_Id_Endereco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            
            parms.Add(new MySqlParameter("Nm_Cliente", cliente.Nome));
            parms.Add(new MySqlParameter("Ds_Email", cliente.Email));
            parms.Add(new MySqlParameter("Dt_Nascimento", cliente.DataNascimento));
            parms.Add(new MySqlParameter("Ds_Cpf", cliente.CPF));
            parms.Add(new MySqlParameter("Ds_Telefone", cliente.Telefone));
            parms.Add(new MySqlParameter("Ds_RG", cliente.RG));
            
            parms.Add(new MySqlParameter("Endereco_Id_Endereco", cliente.IdEndereco));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ClienteDTO cliente)
        {

            string script = @"UPDATE Cliente SET 
                                                 Nm_Cliente = @Nm_Cliente,
                                                 Ds_Email = @Ds_Email; 
                                                 Dt_Nascimento = @Dt_Nascimento,
                                                 Ds_Cpf = @Ds_Cpf,
                                                 Ds_Telefone =  @Ds_Telefone,
                                                 Ds_RG = @Ds_RG,
                                         
                                                Endereco_Id_Endereco = @Endereco_Id_Endereco
                             WHERE Id_Cliente = @Id_Cliente;";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_Cliente", cliente.Nome));
            parms.Add(new MySqlParameter("Ds_Email", cliente.Email));
            parms.Add(new MySqlParameter("Dt_Nascimento", cliente.DataNascimento));
            parms.Add(new MySqlParameter("Ds_Cpf", cliente.CPF));
            parms.Add(new MySqlParameter("Ds_Telefone", cliente.Telefone));
            parms.Add(new MySqlParameter("Ds_RG", cliente.RG));
            parms.Add(new MySqlParameter("Endereco_Id_Endereco", cliente.IdEndereco));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public List<ClienteDTO> Listar()
        {
            string script =
                @"SELECT Nm_Cliente FROM Cliente";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<ClienteDTO> clientes = new List<ClienteDTO>();

            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("Id_Cliente");
                dto.Nome = reader.GetString("Nm_Cliente");
                dto.Email = reader.GetString("Ds_Email");
                dto.DataNascimento = reader.GetDateTime("Dt_Nascimento");
                dto.CPF = reader.GetString("Ds_CPF");
                dto.Telefone = reader.GetString("Ds_Telefone");
                dto.RG = reader.GetString("Ds_RG");
                
                dto.IdEndereco = reader.GetInt32("Endereco_Id_Endereco");
                clientes.Add(dto);

            }

            reader.Close();

            return clientes;

        }

        public ClienteDTO ConsultarPorNome(string nome)
        {
            string script = @"SELECT * FROM Cliente WHERE Nm_Cliente = @Nm_Cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_Cliente", nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ClienteDTO dto = null;
            if (reader.Read())
            {
                dto = new ClienteDTO();
                dto.Id = reader.GetInt32("Id_Cliente");
                dto.Nome = reader.GetString("Nm_Cliente");
                dto.Email = reader.GetString("Ds_Email");
                dto.DataNascimento = reader.GetDateTime("Dt_Nascimento");
                dto.CPF = reader.GetString("Ds_CPF");
                dto.Telefone = reader.GetString("Ds_Telefone");
                dto.RG = reader.GetString("Ds_RG");
                
                dto.IdEndereco = reader.GetInt32("Endereco_Id_Endereco");
            }
            reader.Close();

            return dto;

        }

    }
}
