﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class PedidoItemDatabase
    {
        public int Salvar(PedidoItemDTO pedidoitem)
        {
            string script =
                @"INSERT INTO Pedido_Item(Pedido_Id_Pedido, Produto_Id_Produto)
                    VALUE (@Pedido_Id_Pedido, @Produto_Id_Produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Pedido_Id_Pedido", pedidoitem.IdPedido));
            parms.Add(new MySqlParameter("Produto_Id_Produto", pedidoitem.IdProduto));
            







            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<PedidoItemDTO> Listar()
        {
            string script =
                @"SELECT * FROM Pedido_Item";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<PedidoItemDTO> pedidoItem = new List<PedidoItemDTO>();

            while (reader.Read())
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.Id = reader.GetInt32("Id_Pedido_Item");
                dto.IdPedido = reader.GetInt32("Pedido_Id_Pedido");
                dto.IdProduto = reader.GetInt32("Produto_Id_Produto");
                





                pedidoItem.Add(dto);

            }

            reader.Close();

            return pedidoItem;
        }
    }
}
