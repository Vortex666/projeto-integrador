﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class PedidoDatabase
    {
        public int Salvar(PedidoDTO pedido)
        {
            string script =
                @"INSERT INTO Pedido(Dt_Pedido, Ds_Total, Ds_Forma_Pagamento, Qt_Quantidade, Ds_Descricao, Funcionario_Id_Func, Cliente_Id_Cliente)
                    VALUE (@Dt_Pedido, @Ds_Total, @Ds_Forma_Pagamento, @Qt_Quantidade, @Funcionario_Id_Func, @Cliente_Id_Cliente)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Dt_Pedido", pedido.Data));
            parms.Add(new MySqlParameter("Ds_Total", pedido.Total));
            parms.Add(new MySqlParameter("Ds_Descricao", pedido.Descricao));
            parms.Add(new MySqlParameter("Ds_Forma_Pagamento", pedido.FormaPagamento));
            parms.Add(new MySqlParameter("Qt_Quantidade", pedido.Quantidade));
            parms.Add(new MySqlParameter("Funcionario_Id_Func", pedido.IdFuncionário));
            parms.Add(new MySqlParameter("Cliente_Id_Cliente", pedido.IdCliente));
            






            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<PedidoDTO> Listar()
        {
            string script =
                @"SELECT * FROM Pedido";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<PedidoDTO> pedido = new List<PedidoDTO>();

            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();
                dto.Id = reader.GetInt32("Id_Pedido");
                dto.Data = reader.GetDateTime("Dt_Pedido");
                dto.Total = reader.GetDecimal("Ds_Total");
                dto.Descricao = reader.GetString("Ds_Descricao");
                dto.FormaPagamento = reader.GetString("Ds_Forma_Pagamento");
                dto.Quantidade = reader.GetInt32("Qt_Quantidade");
                dto.IdFuncionário = reader.GetInt32("Funcionario_Id_Func");
                dto.IdCliente = reader.GetInt32("Cliente_Id_Cliente");





                pedido.Add(dto);

            }

            reader.Close();

            return pedido;
        }
    }
}
