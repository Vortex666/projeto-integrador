﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Databases
{
    public class CompraFornecedorDatabase
    {
        public class CompraDatabase
        {
            public int Salvar(CompraFornecedorDTO comprafornecedor)
            {
                string script =
                    @"INSERT INTO Cliente (Compra_Id_Compra, Fornecedor_Id_Fornec)
                    VALUE (@Compra_Id_Compra, @Fornecedor_Id_Fornec)";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("Compra_Id_Compra", comprafornecedor.IdCompra));
                parms.Add(new MySqlParameter("Fornecedor_Id_Fornec", comprafornecedor.IdFornecedor));



                Database db = new Database();
                int pk = db.ExecuteInsertScriptWithPk(script, parms);
                return pk;
            }


            public List<CompraFornecedorDTO> Listar()
            {
                string script =
                    @"SELECT * FROM Compra_Fornec";
                Database db = new Database();
                MySqlDataReader reader = db.ExecuteSelectScript(script, null);

                List<CompraFornecedorDTO> compra = new List<CompraFornecedorDTO>();

                while (reader.Read())
                {
                    CompraFornecedorDTO dto = new CompraFornecedorDTO();
                    dto.Id = reader.GetInt32("Id_Compra_Fornec");
                    dto.IdCompra = reader.GetInt32("Compra_Id_Compra");
                    dto.IdFornecedor = reader.GetInt32("Fornecedor_Id_Fornec");
                    

                    compra.Add(dto);

                }

                reader.Close();
                return compra;

            }

            public void Alterar(CompraFornecedorDTO comprafornec)
            {

                string script = @"UPDATE Compra_Fornec SET 
                                                 Compra_Id_Compra = @Compra_Id_Compra,
                                                 Fornecedor_Id_Fornec = @Fornecedor_Id_Fornec, 
                                                
                                                
                                         
                                                
                             WHERE Id_Compra_Fornec = @Id_Compra_Fornec;";
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("Compra_Id_Compra", comprafornec.IdCompra));
                parms.Add(new MySqlParameter("Fornecedor_Id_Fornec", comprafornec.IdFornecedor));
                

                Database db = new Database();
                db.ExecuteInsertScript(script, parms);

            }

        }
    }
}
