﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class FliperamaDTO
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Jogo { get; set; }

        public int Quantidade { get; set; }

        public decimal Preço { get; set; }

    }
}
