﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class CompraDTO
    {
        public int Id { get; set; }

        public decimal Valor { get; set; }

        public int Quantidade { get; set; }

        public string FormaDePagamento { get; set; }

        public DateTime Data { get; set; }

        public int IdCliente { get; set; }
    }
}
