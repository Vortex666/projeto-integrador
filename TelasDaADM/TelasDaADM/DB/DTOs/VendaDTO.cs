﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class VendaDTO
    {
        public int Id { get; set; }

        public int Quantidade { get; set; }

        public DateTime Data { get; set; }

        public decimal Total { get; set; }

        public string Local { get; set; }

        public int IdConsoleComputador { get; set; }

        public int IdProdutoVenda { get; set; }

    }
}
