﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class PeriodoDTO
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public DateTime Hora { get; set; }

    }
}
