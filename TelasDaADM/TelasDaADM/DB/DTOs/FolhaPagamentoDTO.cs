﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class FolhaPagamentoDTO
    {
        public int Id { get; set; }

        public decimal Salário { get; set; }

        public decimal ValeTransporte { get; set; }

        public decimal ValeAlimentação { get; set; }

        public decimal ValeRefeição { get; set; }

        public decimal Convênio { get; set; }

        public decimal Fgts { get; set; }

        public decimal Inss { get; set; }

        public decimal Irrf { get; set; }

        public int HoraExtra { get; set; }

        public int HoraExtraFeriado { get; set; }

        public decimal SalárioBruto { get; set; }

        public decimal Desconto { get; set; }

        public decimal Sindicato { get; set; }

        public decimal SalárioLíquido { get; set; }

        public string FormaPagamento { get; set; }

        public DateTime Disponível { get; set; }

        public int Atrasos { get; set; }

        public int Faltas { get; set; }

        public double SalárioFamilia { get; set; }

        public decimal DSR { get; set; }

        public int IdFunc { get; set; }

    }
}
