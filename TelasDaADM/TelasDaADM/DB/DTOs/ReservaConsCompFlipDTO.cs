﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class ReservaConsCompFlipDTO
    {
        public int Id { get; set; }

        public int IdReserva { get; set; }

        public int IdConsoleComputador { get; set; }

        public int IdFliperama { get; set; }
    }
}
