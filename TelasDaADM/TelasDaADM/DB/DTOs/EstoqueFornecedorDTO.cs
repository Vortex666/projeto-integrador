﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class EstoqueFornecedorDTO
    {
        public int Id { get; set; }

        public int IdEstoque { get; set; }

        public int IdFornecedor { get; set; }
    }
}
