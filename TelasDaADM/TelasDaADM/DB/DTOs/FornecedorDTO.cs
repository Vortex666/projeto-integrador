﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class FornecedorDTO
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public DateTime DataNascimento { get; set; }

        public string CPFCNPJ { get; set; }

        public string RG { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        public int IdFliperama { get; set; }

        public int IdProduto { get; set; }

        public int IdConsoleComputador { get; set; }

        public int IdEndereco { get; set; }
    }
}
