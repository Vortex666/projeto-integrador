﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class VendasFornecedorDTO
    {
        public int Id { get; set; }

        public int IdVenda { get; set; }

        public int IdFornecedor { get; set; }
    }
}
