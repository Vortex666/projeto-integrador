﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class ProdutoDTO
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public int Quantidade { get; set; }

        public decimal Preco { get; set; }

        public string Descricao { get; set; }

        public string Marca { get; set;  }

        public string Modelo { get; set; }

        public decimal Desconto { get; set; }
    }
}
