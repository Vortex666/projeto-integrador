﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class CompraFornecedorDTO
    {
        public int Id { get; set; }

        public int IdCompra { get; set; }

        public int IdFornecedor { get; set; }
    }
}
