﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class PedidoDTO
    {
        public int Id { get; set; }

        public DateTime Data { get; set; }

        public decimal Total { get; set; }

        public string FormaPagamento { get; set; }

        public int Quantidade { get; set; }

        public string Descricao { get; set; }

        public int IdCliente { get; set; }

        public int IdFuncionário { get; set; }
    }
}
