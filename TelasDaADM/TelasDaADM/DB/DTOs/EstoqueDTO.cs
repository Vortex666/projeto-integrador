﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class EstoqueDTO
    {
        public int Id { get; set; }

        public string Local { get; set; }

        public int Minimo { get; set; }

        public int Maximo { get; set; }

        public int Disponivel { get; set; }

        public int IdProdutoVenda { get; set; }

        public int IdConsoleComputador { get; set; }

    }
}
