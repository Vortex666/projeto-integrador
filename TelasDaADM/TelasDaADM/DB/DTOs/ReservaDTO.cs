﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    public class ReservaDTO
    {
        public int Id { get; set; }

        public DateTime Data { get; set; }

        public int IdFicha { get; set; }

        public int IdPeriodo { get; set; }

        public int IdCliente { get; set; }
    }
}
