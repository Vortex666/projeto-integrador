﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasDaADM.DB.DTOs
{
    class ConsoleComputadorDTO
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Marca { get; set; }
        
        public string PecasAdicionais { get; set; }

        public string Configuracoes { get; set; }

        public int Quantidade { get; set; }

        public decimal PrecoTotal { get; set; }

        public decimal PrecoPcConsole { get; set; }

    }
}
