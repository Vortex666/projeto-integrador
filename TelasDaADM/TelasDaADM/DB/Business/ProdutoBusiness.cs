﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.Databases;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Business
{
    public class ProdutoBusiness
    {

        public int Salvar(ProdutoDTO produto)
        {
            ProdutoDatabase produtoDB = new ProdutoDatabase();
            int id = produtoDB.Salvar(produto);
            return id;
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase produtoDB = new ProdutoDatabase();
            List<ProdutoDTO> produtos = produtoDB.Listar();

            return produtos;
        }
    }
}
