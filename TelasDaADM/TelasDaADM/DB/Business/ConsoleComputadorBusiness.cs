﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.Databases;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Business
{
    class ConsoleComputadorBusiness
    {

        public int Salvar(ConsoleComputadorDTO consoleComputador)
        {
            ConsoleComputadorDatabase conspcDB = new ConsoleComputadorDatabase();
            int id = conspcDB.Salvar(consoleComputador);
            return id;
        }
        
        public List<ConsoleComputadorDTO> Listar()
        {
            ConsoleComputadorDatabase conspcDB = new ConsoleComputadorDatabase();
            List<ConsoleComputadorDTO> conspc = conspcDB.Listar();

            return conspc;
        }
    }
}
