﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.Databases;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Business
{
    class FliperamaBusiness
    {
        public int Salvar(FliperamaDTO flip)
        {
            FliperamaDatabase flipDB = new FliperamaDatabase();
            int id = flipDB.Salvar(flip);
            return id;
        }

        public List<FliperamaDTO> Listar()
        {
            FliperamaDatabase flipDB = new FliperamaDatabase();
            List<FliperamaDTO> flip = flipDB.Listar();

            return flip;
        }
    }
}
