﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.Databases;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Business
{
    class FichaBusiness
    {

        public int Salvar (FichaDTO ficha)
        {
            FichaDatabase fichaDB = new FichaDatabase();
            int id = fichaDB.Salvar(ficha);
            return id;
        }
    }
}
