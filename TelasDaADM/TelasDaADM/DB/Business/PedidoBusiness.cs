﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.Databases;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB
{
    public class PedidoBusiness
    {

        public int Salvar(PedidoDTO pedido)
        {
            PedidoDatabase pedidoDB = new PedidoDatabase();
            int id = pedidoDB.Salvar(pedido);

            return id;
        }
    }
}
