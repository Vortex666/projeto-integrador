﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.Databases;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Business
{
    public class EnderecoBusiness
    {

        public int Salvar(EnderecoDTO endereco)
        {
            EnderecoDatabase enderecoDB = new EnderecoDatabase();
            int id = enderecoDB.Salvar(endereco);
            return id;
        }
    }
}
