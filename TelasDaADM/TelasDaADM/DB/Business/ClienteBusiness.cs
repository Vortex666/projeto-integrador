﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.Databases;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Business
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO cliente)
        {
            ClienteDatabase clienteDB = new ClienteDatabase();
            int id = clienteDB.Salvar(cliente);
            return id;
        }
        
        public List<ClienteDTO> Listar()
        {
            ClienteDatabase clienteDB = new ClienteDatabase();
            List<ClienteDTO> cliente = clienteDB.Listar();

            return cliente;
        }
    }
}
