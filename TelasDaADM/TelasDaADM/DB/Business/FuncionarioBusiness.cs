﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.Databases;
using TelasDaADM.DB.DTOs;


namespace TelasDaADM.DB.Business
{
    class FuncionarioBusiness
    {
        public FuncionarioDTO Logar(string login, string senha)
        {
            if (login == string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório.");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatória.");
            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Logar(login, senha);

        }

        public int Salvar(FuncionarioDTO func)
        {
            FuncionarioDatabase funcDB = new FuncionarioDatabase();
            int id = funcDB.Salvar(func);
            return id;
        }

        public void Alterar(FuncionarioDTO Funcionario)
        {
            FuncionarioDatabase func = new FuncionarioDatabase();
            func.Alterar(Funcionario);

        }

        public void Remover(int idFunc)
        {
            FuncionarioDatabase funcDB = new FuncionarioDatabase();
             funcDB.Remover(idFunc);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase funcDB = new FuncionarioDatabase();


            return funcDB.Listar();
        }

           
        
               
    }
}
