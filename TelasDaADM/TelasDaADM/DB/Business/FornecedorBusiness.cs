﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.Databases;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Business
{
    class FornecedorBusiness
    {
        public int Salvar(FornecedorDTO fornec)
        {
            FornecedorDatabase fornecDB = new FornecedorDatabase();
            int id = fornecDB.Salvar(fornec);
            return id;
        }
    }
}
