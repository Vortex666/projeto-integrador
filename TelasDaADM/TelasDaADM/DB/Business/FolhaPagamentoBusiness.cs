﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelasDaADM.DB.Databases;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.DB.Business
{
    public class FolhaPagamentoBusiness
    {

        public List<FolhaPagamentoDTO> Listar()
        {
            FolhaPagamentoDatabase folhaDB = new FolhaPagamentoDatabase();
            List<FolhaPagamentoDTO> folhas = folhaDB.Listar();
            return folhas;
        }
    }
}
