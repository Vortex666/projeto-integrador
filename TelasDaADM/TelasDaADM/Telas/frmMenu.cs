﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.Cadastro;
using TelasDaADM.Consultar;
using TelasDaADM.Telas;
using TelasDaADM.Telas.Cadastro;
using TelasDaADM.Telas.Consultar;

namespace TelasDaADM
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            
            
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {

        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();

            frmCadastrarCliente clientetela = new frmCadastrarCliente();

            clientetela.Show();

            
        }

        private void tsmiFuncionarioCadastro_Click(object sender, EventArgs e)
        {
            this.Hide();

            frmCadastrarFuncionario funcionariotela = new frmCadastrarFuncionario();

            funcionariotela.Show();

            
        }

        private void tsmiReservaCadastro_Click(object sender, EventArgs e)
        {

            this.Hide();
            frmCadastroReserva reservatela = new frmCadastroReserva();

            reservatela.Show();

           
        }

        private void tsmiPedidoCadastro_Click(object sender, EventArgs e)
        {

            this.Hide();
            frmCadastrarPedido pedidotela = new frmCadastrarPedido();

            pedidotela.Show();

            
        }

        private void tsmiProdutoCadastro_Click(object sender, EventArgs e)
        {

            this.Hide();
            frmCadastrarProduto produtotela = new frmCadastrarProduto();

            produtotela.Show();

            
        }

        private void tsmiFornecedorCadastro_Click(object sender, EventArgs e)
        {

            this.Hide();
            frmCadastroFornecedor fornecedortela = new frmCadastroFornecedor();

            fornecedortela.Show();

            
        }

        private void tsmiFolhaPagamentoCadastro_Click(object sender, EventArgs e)
        {

            this.Hide();
            frmCadastroFolhadePagamento folhatela = new frmCadastroFolhadePagamento();

            folhatela.Show();

            
        }

        private void tsmiCompraCadastro_Click(object sender, EventArgs e)
        {

            this.Hide();
            frmCadastroCompras compratela = new frmCadastroCompras();

            compratela.Show();

            
        }

        private void tsmiVendaCadastro_Click(object sender, EventArgs e)
        {

            this.Hide();
            frmCadastroVendas vendastelas = new frmCadastroVendas();

            vendastelas.Show();

            

           
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();

            Application.Exit();
        }

        private void logoffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();

            frmLogin logintela = new frmLogin();
            logintela.Show();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();

            Application.Exit();
        }

        private void funcionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.Hide();


            frmConsultarFuncionario funcionariotela = new frmConsultarFuncionario();

            funcionariotela.Show();


        }

        private void tsmiReservaConsulta_Click(object sender, EventArgs e)
        {
            this.Hide();

            frmConsultarReserva reservatela = new frmConsultarReserva();

            reservatela.Show();

        }

        private void tsmiPedidoConsulta_Click(object sender, EventArgs e)
        {
            this.Hide();

            frmConsultarPedido pedidotela = new frmConsultarPedido();

            pedidotela.Show();

        }

        private void tsmiProdutoConsultar_Click(object sender, EventArgs e)
        {
            this.Hide();

            frmConsultarProduto produtotela = new frmConsultarProduto();

            produtotela.Show();

        }

        private void tsmiFornecedorConsulta_Click(object sender, EventArgs e)
        {
            this.Hide();

            frmConsultarFornecedor fornecedortela = new frmConsultarFornecedor();

            fornecedortela.Show();

        }

        private void tsmiFolhadePagamentoConsulta_Click(object sender, EventArgs e)
        {
            this.Hide();

            frmConsultarFolhadePagamento folhatela = new frmConsultarFolhadePagamento();

            folhatela.Show();

        }

        private void tsmiCompraConsulta_Click(object sender, EventArgs e)
        {
           

           
        }

        private void tsbEstoque_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmEstoque estoque = new frmEstoque();
            estoque.Show();

        }

        private void tsmCaixa_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCaixa caixa = new frmCaixa();
            caixa.Show();
        }

        private void consolePcFliperamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastrarConsolePc conspc = new frmCadastrarConsolePc();
            conspc.Show();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void tsmiClienteCadastro_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastrarCliente cliente = new frmCadastrarCliente();
            cliente.Show();

        }

        private void tsmiFuncionarioCadastro_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastrarFuncionario func = new frmCadastrarFuncionario();
            func.Show();
        }

        private void tsmiFornecedorCadastro_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastroFornecedor fornec = new frmCadastroFornecedor();
            fornec.Show();
        }

        private void tsmiFuncionarioConsulta_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarFuncionario func = new frmConsultarFuncionario();
            func.Show();

        }

        private void tsmiReservaCadastro_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastroReserva reserva = new frmCadastroReserva();
            reserva.Show();
        }

        private void tsmiPedidoCadastro_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastrarPedido pedido = new frmCadastrarPedido();
            pedido.Show();
        }

        private void tsmiProdutoCadastro_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastrarProduto produto = new frmCadastrarProduto();
            produto.Show();
        }

        private void tsmiFolhaPagamentoCadastro_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastroFolhadePagamento folha = new frmCadastroFolhadePagamento();
            folha.Show();

        }

        private void tsmiCompraCadastro_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastroCompras compra = new frmCadastroCompras();
            compra.Show();

        }

        private void tsmiVendaCadastro_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastroVendas venda = new frmCadastroVendas();
            venda.Show();

        }

        private void produtoVendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastrarProdutoVenda produtoVenda= new frmCadastrarProdutoVenda();
            produtoVenda.Show();

        }

        private void fliperamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void tsmiReservaConsulta_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarReserva reserva = new frmConsultarReserva();
            reserva.Show();
        }

        private void tsmiPedidoConsulta_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarPedido pedido = new frmConsultarPedido();
            pedido.Show();
        }

        private void tsmiProdutoConsultar_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarProduto produto = new frmConsultarProduto();
            produto.Show();

        }

        private void tsmiFornecedorConsulta_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarFornecedor fornec = new frmConsultarFornecedor();
            fornec.Show();

        }

        private void tsmiFolhadePagamentoConsulta_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarFolhadePagamento folhadePagamento = new frmConsultarFolhadePagamento();
            folhadePagamento.Show();
        }

        private void tsmiCompraConsulta_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarCompra compra = new frmConsultarCompra();
            compra.Show();
        }

        private void tsmiVendaConsulta_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarVenda venda = new frmConsultarVenda();
            venda.Show();
        }

        private void produtoVendaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();

        }

        private void consolePcToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            
        }

        private void fliperamaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void toolStripSplitButton4_Click(object sender, EventArgs e)
        {
            this.Hide();

            FrmTorneio torneio = new FrmTorneio();
            torneio.Show();
        }
    }
}
