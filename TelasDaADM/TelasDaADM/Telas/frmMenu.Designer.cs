﻿namespace TelasDaADM
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.btnSair = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.tsmiClienteCadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFuncionarioCadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReservaCadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPedidoCadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiProdutoCadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFornecedorCadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFolhaPagamentoCadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCompraCadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVendaCadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.consolePcFliperamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtoVendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fliperamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSplitButton2 = new System.Windows.Forms.ToolStripSplitButton();
            this.tsmiFuncionarioConsulta = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReservaConsulta = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPedidoConsulta = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiProdutoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFornecedorConsulta = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFolhadePagamentoConsulta = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCompraConsulta = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVendaConsulta = new System.Windows.Forms.ToolStripMenuItem();
            this.produtoVendaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consolePcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fliperamaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSplitButton3 = new System.Windows.Forms.ToolStripSplitButton();
            this.tsmiSair = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLogoff = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbEstoque = new System.Windows.Forms.ToolStripButton();
            this.tsmCaixa = new System.Windows.Forms.ToolStripButton();
            this.toolStripSplitButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSair
            // 
            this.btnSair.BackColor = System.Drawing.Color.DimGray;
            this.btnSair.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSair.Location = new System.Drawing.Point(12, 255);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(126, 42);
            this.btnSair.TabIndex = 11;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = false;
            this.btnSair.Click += new System.EventHandler(this.button1_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton1,
            this.toolStripSplitButton2,
            this.toolStripSplitButton3,
            this.tsbEstoque,
            this.tsmCaixa,
            this.toolStripSplitButton4});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 12;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiClienteCadastro,
            this.tsmiFuncionarioCadastro,
            this.tsmiReservaCadastro,
            this.tsmiPedidoCadastro,
            this.tsmiProdutoCadastro,
            this.tsmiFornecedorCadastro,
            this.tsmiFolhaPagamentoCadastro,
            this.tsmiCompraCadastro,
            this.tsmiVendaCadastro,
            this.consolePcFliperamaToolStripMenuItem,
            this.produtoVendaToolStripMenuItem,
            this.fliperamaToolStripMenuItem});
            this.toolStripSplitButton1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(89, 22);
            this.toolStripSplitButton1.Text = "Cadastrar";
            // 
            // tsmiClienteCadastro
            // 
            this.tsmiClienteCadastro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiClienteCadastro.Name = "tsmiClienteCadastro";
            this.tsmiClienteCadastro.Size = new System.Drawing.Size(214, 22);
            this.tsmiClienteCadastro.Text = "Cliente";
            this.tsmiClienteCadastro.Click += new System.EventHandler(this.tsmiClienteCadastro_Click);
            // 
            // tsmiFuncionarioCadastro
            // 
            this.tsmiFuncionarioCadastro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiFuncionarioCadastro.Name = "tsmiFuncionarioCadastro";
            this.tsmiFuncionarioCadastro.Size = new System.Drawing.Size(214, 22);
            this.tsmiFuncionarioCadastro.Text = "Funcionário";
            this.tsmiFuncionarioCadastro.Click += new System.EventHandler(this.tsmiFuncionarioCadastro_Click_1);
            // 
            // tsmiReservaCadastro
            // 
            this.tsmiReservaCadastro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiReservaCadastro.Name = "tsmiReservaCadastro";
            this.tsmiReservaCadastro.Size = new System.Drawing.Size(214, 22);
            this.tsmiReservaCadastro.Text = "Reserva";
            this.tsmiReservaCadastro.Click += new System.EventHandler(this.tsmiReservaCadastro_Click_1);
            // 
            // tsmiPedidoCadastro
            // 
            this.tsmiPedidoCadastro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiPedidoCadastro.Name = "tsmiPedidoCadastro";
            this.tsmiPedidoCadastro.Size = new System.Drawing.Size(214, 22);
            this.tsmiPedidoCadastro.Text = "Pedido";
            this.tsmiPedidoCadastro.Click += new System.EventHandler(this.tsmiPedidoCadastro_Click_1);
            // 
            // tsmiProdutoCadastro
            // 
            this.tsmiProdutoCadastro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiProdutoCadastro.Name = "tsmiProdutoCadastro";
            this.tsmiProdutoCadastro.Size = new System.Drawing.Size(214, 22);
            this.tsmiProdutoCadastro.Text = "Produto";
            this.tsmiProdutoCadastro.Click += new System.EventHandler(this.tsmiProdutoCadastro_Click_1);
            // 
            // tsmiFornecedorCadastro
            // 
            this.tsmiFornecedorCadastro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiFornecedorCadastro.Name = "tsmiFornecedorCadastro";
            this.tsmiFornecedorCadastro.Size = new System.Drawing.Size(214, 22);
            this.tsmiFornecedorCadastro.Text = "Fornecedor";
            this.tsmiFornecedorCadastro.Click += new System.EventHandler(this.tsmiFornecedorCadastro_Click_1);
            // 
            // tsmiFolhaPagamentoCadastro
            // 
            this.tsmiFolhaPagamentoCadastro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiFolhaPagamentoCadastro.Name = "tsmiFolhaPagamentoCadastro";
            this.tsmiFolhaPagamentoCadastro.Size = new System.Drawing.Size(214, 22);
            this.tsmiFolhaPagamentoCadastro.Text = "Folha de Pagamento";
            this.tsmiFolhaPagamentoCadastro.Click += new System.EventHandler(this.tsmiFolhaPagamentoCadastro_Click_1);
            // 
            // tsmiCompraCadastro
            // 
            this.tsmiCompraCadastro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiCompraCadastro.Name = "tsmiCompraCadastro";
            this.tsmiCompraCadastro.Size = new System.Drawing.Size(214, 22);
            this.tsmiCompraCadastro.Text = "Compra";
            this.tsmiCompraCadastro.Click += new System.EventHandler(this.tsmiCompraCadastro_Click_1);
            // 
            // tsmiVendaCadastro
            // 
            this.tsmiVendaCadastro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiVendaCadastro.Name = "tsmiVendaCadastro";
            this.tsmiVendaCadastro.Size = new System.Drawing.Size(214, 22);
            this.tsmiVendaCadastro.Text = "Venda";
            this.tsmiVendaCadastro.Click += new System.EventHandler(this.tsmiVendaCadastro_Click_1);
            // 
            // consolePcFliperamaToolStripMenuItem
            // 
            this.consolePcFliperamaToolStripMenuItem.Name = "consolePcFliperamaToolStripMenuItem";
            this.consolePcFliperamaToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.consolePcFliperamaToolStripMenuItem.Text = "Console/Pc";
            this.consolePcFliperamaToolStripMenuItem.Click += new System.EventHandler(this.consolePcFliperamaToolStripMenuItem_Click);
            // 
            // produtoVendaToolStripMenuItem
            // 
            this.produtoVendaToolStripMenuItem.Name = "produtoVendaToolStripMenuItem";
            this.produtoVendaToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.produtoVendaToolStripMenuItem.Text = "Produto Venda";
            this.produtoVendaToolStripMenuItem.Click += new System.EventHandler(this.produtoVendaToolStripMenuItem_Click);
            // 
            // fliperamaToolStripMenuItem
            // 
            this.fliperamaToolStripMenuItem.Name = "fliperamaToolStripMenuItem";
            this.fliperamaToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.fliperamaToolStripMenuItem.Text = "Fliperama";
            this.fliperamaToolStripMenuItem.Click += new System.EventHandler(this.fliperamaToolStripMenuItem_Click);
            // 
            // toolStripSplitButton2
            // 
            this.toolStripSplitButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFuncionarioConsulta,
            this.tsmiReservaConsulta,
            this.tsmiPedidoConsulta,
            this.tsmiProdutoConsultar,
            this.tsmiFornecedorConsulta,
            this.tsmiFolhadePagamentoConsulta,
            this.tsmiCompraConsulta,
            this.tsmiVendaConsulta,
            this.produtoVendaToolStripMenuItem1,
            this.consolePcToolStripMenuItem,
            this.fliperamaToolStripMenuItem1});
            this.toolStripSplitButton2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripSplitButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton2.Image")));
            this.toolStripSplitButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton2.Name = "toolStripSplitButton2";
            this.toolStripSplitButton2.Size = new System.Drawing.Size(86, 22);
            this.toolStripSplitButton2.Text = "Consultar";
            // 
            // tsmiFuncionarioConsulta
            // 
            this.tsmiFuncionarioConsulta.Name = "tsmiFuncionarioConsulta";
            this.tsmiFuncionarioConsulta.Size = new System.Drawing.Size(214, 22);
            this.tsmiFuncionarioConsulta.Text = "Funcionário";
            this.tsmiFuncionarioConsulta.Click += new System.EventHandler(this.tsmiFuncionarioConsulta_Click);
            // 
            // tsmiReservaConsulta
            // 
            this.tsmiReservaConsulta.Name = "tsmiReservaConsulta";
            this.tsmiReservaConsulta.Size = new System.Drawing.Size(214, 22);
            this.tsmiReservaConsulta.Text = "Reserva";
            this.tsmiReservaConsulta.Click += new System.EventHandler(this.tsmiReservaConsulta_Click_1);
            // 
            // tsmiPedidoConsulta
            // 
            this.tsmiPedidoConsulta.Name = "tsmiPedidoConsulta";
            this.tsmiPedidoConsulta.Size = new System.Drawing.Size(214, 22);
            this.tsmiPedidoConsulta.Text = "Pedido";
            this.tsmiPedidoConsulta.Click += new System.EventHandler(this.tsmiPedidoConsulta_Click_1);
            // 
            // tsmiProdutoConsultar
            // 
            this.tsmiProdutoConsultar.Name = "tsmiProdutoConsultar";
            this.tsmiProdutoConsultar.Size = new System.Drawing.Size(214, 22);
            this.tsmiProdutoConsultar.Text = "Produto";
            this.tsmiProdutoConsultar.Click += new System.EventHandler(this.tsmiProdutoConsultar_Click_1);
            // 
            // tsmiFornecedorConsulta
            // 
            this.tsmiFornecedorConsulta.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiFornecedorConsulta.Name = "tsmiFornecedorConsulta";
            this.tsmiFornecedorConsulta.Size = new System.Drawing.Size(214, 22);
            this.tsmiFornecedorConsulta.Text = "Fornecedor";
            this.tsmiFornecedorConsulta.Click += new System.EventHandler(this.tsmiFornecedorConsulta_Click_1);
            // 
            // tsmiFolhadePagamentoConsulta
            // 
            this.tsmiFolhadePagamentoConsulta.Name = "tsmiFolhadePagamentoConsulta";
            this.tsmiFolhadePagamentoConsulta.Size = new System.Drawing.Size(214, 22);
            this.tsmiFolhadePagamentoConsulta.Text = "Folha de Pagamento";
            this.tsmiFolhadePagamentoConsulta.Click += new System.EventHandler(this.tsmiFolhadePagamentoConsulta_Click_1);
            // 
            // tsmiCompraConsulta
            // 
            this.tsmiCompraConsulta.Name = "tsmiCompraConsulta";
            this.tsmiCompraConsulta.Size = new System.Drawing.Size(214, 22);
            this.tsmiCompraConsulta.Text = "Compra";
            this.tsmiCompraConsulta.Click += new System.EventHandler(this.tsmiCompraConsulta_Click_1);
            // 
            // tsmiVendaConsulta
            // 
            this.tsmiVendaConsulta.Name = "tsmiVendaConsulta";
            this.tsmiVendaConsulta.Size = new System.Drawing.Size(214, 22);
            this.tsmiVendaConsulta.Text = "Venda";
            this.tsmiVendaConsulta.Click += new System.EventHandler(this.tsmiVendaConsulta_Click);
            // 
            // produtoVendaToolStripMenuItem1
            // 
            this.produtoVendaToolStripMenuItem1.Name = "produtoVendaToolStripMenuItem1";
            this.produtoVendaToolStripMenuItem1.Size = new System.Drawing.Size(214, 22);
            this.produtoVendaToolStripMenuItem1.Text = "Produto Venda";
            this.produtoVendaToolStripMenuItem1.Click += new System.EventHandler(this.produtoVendaToolStripMenuItem1_Click);
            // 
            // consolePcToolStripMenuItem
            // 
            this.consolePcToolStripMenuItem.Name = "consolePcToolStripMenuItem";
            this.consolePcToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.consolePcToolStripMenuItem.Text = "Console/Pc";
            this.consolePcToolStripMenuItem.Click += new System.EventHandler(this.consolePcToolStripMenuItem_Click);
            // 
            // fliperamaToolStripMenuItem1
            // 
            this.fliperamaToolStripMenuItem1.Name = "fliperamaToolStripMenuItem1";
            this.fliperamaToolStripMenuItem1.Size = new System.Drawing.Size(214, 22);
            this.fliperamaToolStripMenuItem1.Text = "Fliperama";
            this.fliperamaToolStripMenuItem1.Click += new System.EventHandler(this.fliperamaToolStripMenuItem1_Click);
            // 
            // toolStripSplitButton3
            // 
            this.toolStripSplitButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSair,
            this.tsmiLogoff});
            this.toolStripSplitButton3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripSplitButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton3.Image")));
            this.toolStripSplitButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton3.Name = "toolStripSplitButton3";
            this.toolStripSplitButton3.Size = new System.Drawing.Size(79, 22);
            this.toolStripSplitButton3.Text = "Arquivos";
            // 
            // tsmiSair
            // 
            this.tsmiSair.Name = "tsmiSair";
            this.tsmiSair.Size = new System.Drawing.Size(117, 22);
            this.tsmiSair.Text = "Sair";
            // 
            // tsmiLogoff
            // 
            this.tsmiLogoff.Name = "tsmiLogoff";
            this.tsmiLogoff.Size = new System.Drawing.Size(117, 22);
            this.tsmiLogoff.Text = "Logoff";
            // 
            // tsbEstoque
            // 
            this.tsbEstoque.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbEstoque.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbEstoque.Image = ((System.Drawing.Image)(resources.GetObject("tsbEstoque.Image")));
            this.tsbEstoque.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEstoque.Name = "tsbEstoque";
            this.tsbEstoque.Size = new System.Drawing.Size(63, 22);
            this.tsbEstoque.Text = "Estoque";
            this.tsbEstoque.Click += new System.EventHandler(this.tsbEstoque_Click);
            // 
            // tsmCaixa
            // 
            this.tsmCaixa.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsmCaixa.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmCaixa.Image = ((System.Drawing.Image)(resources.GetObject("tsmCaixa.Image")));
            this.tsmCaixa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsmCaixa.Name = "tsmCaixa";
            this.tsmCaixa.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tsmCaixa.Size = new System.Drawing.Size(50, 22);
            this.tsmCaixa.Text = "Caixa";
            this.tsmCaixa.Click += new System.EventHandler(this.tsmCaixa_Click);
            // 
            // toolStripSplitButton4
            // 
            this.toolStripSplitButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButton4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripSplitButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton4.Image")));
            this.toolStripSplitButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton4.Name = "toolStripSplitButton4";
            this.toolStripSplitButton4.Size = new System.Drawing.Size(58, 22);
            this.toolStripSplitButton4.Text = "Torneio";
            this.toolStripSplitButton4.Click += new System.EventHandler(this.toolStripSplitButton4_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 309);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.btnSair);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Flip\'s House";
            this.Load += new System.EventHandler(this.frmMenu_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem tsmiClienteCadastro;
        private System.Windows.Forms.ToolStripMenuItem tsmiFuncionarioCadastro;
        private System.Windows.Forms.ToolStripMenuItem tsmiReservaCadastro;
        private System.Windows.Forms.ToolStripMenuItem tsmiPedidoCadastro;
        private System.Windows.Forms.ToolStripMenuItem tsmiProdutoCadastro;
        private System.Windows.Forms.ToolStripMenuItem tsmiFornecedorCadastro;
        private System.Windows.Forms.ToolStripMenuItem tsmiFolhaPagamentoCadastro;
        private System.Windows.Forms.ToolStripMenuItem tsmiCompraCadastro;
        private System.Windows.Forms.ToolStripMenuItem tsmiVendaCadastro;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton2;
        private System.Windows.Forms.ToolStripMenuItem tsmiFuncionarioConsulta;
        private System.Windows.Forms.ToolStripMenuItem tsmiReservaConsulta;
        private System.Windows.Forms.ToolStripMenuItem tsmiPedidoConsulta;
        private System.Windows.Forms.ToolStripMenuItem tsmiProdutoConsultar;
        private System.Windows.Forms.ToolStripMenuItem tsmiFornecedorConsulta;
        private System.Windows.Forms.ToolStripMenuItem tsmiFolhadePagamentoConsulta;
        private System.Windows.Forms.ToolStripMenuItem tsmiCompraConsulta;
        private System.Windows.Forms.ToolStripMenuItem tsmiVendaConsulta;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton3;
        private System.Windows.Forms.ToolStripMenuItem tsmiSair;
        private System.Windows.Forms.ToolStripMenuItem tsmiLogoff;
        private System.Windows.Forms.ToolStripButton tsbEstoque;
        private System.Windows.Forms.ToolStripButton tsmCaixa;
        private System.Windows.Forms.ToolStripMenuItem consolePcFliperamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtoVendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fliperamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtoVendaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consolePcToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fliperamaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton toolStripSplitButton4;
    }
}