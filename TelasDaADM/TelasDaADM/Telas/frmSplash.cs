﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelasDaADM
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();

            Task.Factory.StartNew(() =>
            {
               
                System.Threading.Thread.Sleep(5000);

                

                Invoke(new Action(() =>
                {
                   
                    frmLogin frm = new frmLogin();
                    frm.Show();
                    Hide();
                }));
            });

        }
            


    }
}
