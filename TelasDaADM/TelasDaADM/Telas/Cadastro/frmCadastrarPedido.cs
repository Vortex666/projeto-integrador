﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.DB;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM
{
    public partial class frmCadastrarPedido : Form
    {
        public frmCadastrarPedido()
        {
            InitializeComponent();
            CarregarCombos();
        }

        private void frmCadastrarPedido_Load(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                PedidoDTO pedido = new PedidoDTO();
                pedido.Data = dtpData.Value;
                pedido.Total = Convert.ToDecimal(txtTotal.Text);
                pedido.FormaPagamento = cboFormaPagamento.Text;
                pedido.Descricao = txtDescricao.Text;

                ClienteDTO cliente = cboCliente.SelectedItem as ClienteDTO;
                FuncionarioDTO func = cboFuncionario.SelectedItem as FuncionarioDTO;
                ProdutoDTO produto = cboProduto.SelectedItem as ProdutoDTO;

                PedidoBusiness business = new PedidoBusiness();
                pedido.Id = business.Salvar(pedido);

                MessageBox.Show("Pedido feito com Sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Flip's House",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {


                MessageBox.Show("Deu algo de errado, por favor contatar o Administrador", "Flip's House",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        void CarregarCombos()
        {
            ClienteBusiness clienteBusiness = new ClienteBusiness();
            List<ClienteDTO> cliente = clienteBusiness.Listar();

            cboCliente.ValueMember = nameof(ClienteDTO.Id);
            cboCliente.DisplayMember = nameof(ClienteDTO.Nome);
            cboCliente.DataSource = cliente;

            FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness();
            List<FuncionarioDTO> funcionarios = funcionarioBusiness.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.Id);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFuncionario.DataSource = funcionarios;

            ProdutoBusiness produtoBusiness = new ProdutoBusiness();
            List<ProdutoDTO> produtos = produtoBusiness.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = produtos;
            ProdutoDTO produto = new ProdutoDTO();

            nudQtd.Value = produto.Quantidade;
            lblPreço.Text = Convert.ToString(produto.Preco);


        }

        private void cboFormaPagamento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
