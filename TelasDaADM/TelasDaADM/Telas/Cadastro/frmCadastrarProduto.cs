﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.Cadastro
{
    public partial class frmCadastrarProduto : Form
    {
        public frmCadastrarProduto()
        {
            InitializeComponent();
        }

        private void frmCadastrarProduto_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO produto = new ProdutoDTO();
                produto.Nome = txtNome.Text;
                produto.Marca = txtMarca.Text;
                produto.Preco = Convert.ToDecimal(txtPreco.Text);
                produto.Modelo = txtModelo.Text;
                produto.Quantidade = Convert.ToInt32(nudQtd.Value);
                produto.Descricao = txtDescricao.Text;

                ProdutoBusiness business = new ProdutoBusiness();
                produto.Id = business.Salvar(produto);

                MessageBox.Show("Produto Cadastrado com Sucesso", "Flip's House", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Flip's House",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception af)
            {


                MessageBox.Show("Deu algo de errado, por favor contatar o Administrador " + af.Message, "Flip's House",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }





        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Application.Exit();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
