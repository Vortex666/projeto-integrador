﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.Cadastro
{
    public partial class frmCadastroReserva : Form
    {
        public frmCadastroReserva()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FichaDTO ficha = new FichaDTO();
            ficha.Preço = Convert.ToDecimal(txtPreco.Text);
            ficha.Quantidade = Convert.ToInt32(nudQtd.Value);
            FichaBusiness business = new FichaBusiness();
            business.Salvar(ficha);
            PeriodoDTO periodo = new PeriodoDTO();
            periodo.Nome = cboPeríodo.SelectedText;
            periodo.Hora = Convert.ToDateTime(txtHoraInicio.Text);
            ReservaDTO reserva = new ReservaDTO();
            reserva.Data = dtpData.Value;
            
        }
    }
}
