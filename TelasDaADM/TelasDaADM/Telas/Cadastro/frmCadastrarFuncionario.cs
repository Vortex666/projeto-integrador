﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;
using TelasDaADM.Telas.Cadastro;

namespace TelasDaADM.Cadastro
{
    public partial class frmCadastrarFuncionario : Form
    {
        public frmCadastrarFuncionario()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                EnderecoDTO endereco = new EnderecoDTO();
                endereco.Bairro = txtBairro.Text;
                endereco.CEP = txtCep.Text;
                endereco.Cidade = txtCidade.Text;
                endereco.Complemento = txtComplemento.Text;
                EnderecoBusiness busines = new EnderecoBusiness();
                endereco.Id = busines.Salvar(endereco);



                FuncionarioDTO func = new FuncionarioDTO();
                func.Nome = txtNome.Text;
                func.DataNascimento = dtpNascimento.Value;
                func.Cargo = txtCargo.Text;
                func.Telefone = txtTelefone.Text;
                func.CPF = txtCpf.Text;
                func.Email = txtEmail.Text;
                func.Observacao = txtObservacao.Text;
                func.RG = txtRG.Text;
                func.Salario = Convert.ToDecimal(txtSalario.Text);
                func.Login = txtUsuario.Text;
                func.Senha = txtSenha.Text;
                func.Admin = cboAdmin.SelectedText;
                func.IdEndereco = endereco.Id;
                FuncionarioBusiness business = new FuncionarioBusiness();
                func.Id = business.Salvar(func);










                MessageBox.Show("Funcionário cadastrado com sucesso.", "Flip's House", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Flip's House",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception af)
            {


                MessageBox.Show("Deu algo de errado, por favor contatar o Administrador " + af.Message, "Flip's House",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }



        }


        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void frmCadastrarFuncionário_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }
    }
}
