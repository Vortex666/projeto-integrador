﻿namespace TelasDaADM
{
    partial class frmCadastroFolhadePagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastroFolhadePagamento));
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtHoraExtra = new System.Windows.Forms.TextBox();
            this.rbnNaoHEFerias = new System.Windows.Forms.RadioButton();
            this.rbnSimHEFerias = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtFGTS = new System.Windows.Forms.MaskedTextBox();
            this.txtINSS = new System.Windows.Forms.MaskedTextBox();
            this.txtSalarioBruto = new System.Windows.Forms.MaskedTextBox();
            this.txtSalarioLiquido = new System.Windows.Forms.MaskedTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.txtValeAlimentacao = new System.Windows.Forms.MaskedTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.txtValeRefeição = new System.Windows.Forms.MaskedTextBox();
            this.txtValeTransporte = new System.Windows.Forms.MaskedTextBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtSalarioFamilia = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.cboFunc = new System.Windows.Forms.ComboBox();
            this.txtIRRF = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDSR = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.nudAtrasos = new System.Windows.Forms.NumericUpDown();
            this.nudFaltas = new System.Windows.Forms.NumericUpDown();
            this.rbnSimHE = new System.Windows.Forms.RadioButton();
            this.rbnNaoHE = new System.Windows.Forms.RadioButton();
            this.txtHoraExtraFeriado = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtConvênio = new System.Windows.Forms.MaskedTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblResultadoHoras = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblDSR = new System.Windows.Forms.Label();
            this.lblIRRF = new System.Windows.Forms.Label();
            this.lblINSS = new System.Windows.Forms.Label();
            this.lblFGTS = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblResultado = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSalário = new System.Windows.Forms.MaskedTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.dtpDisponivel = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.cboFormaPagamento = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAtrasos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltas)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label1.Location = new System.Drawing.Point(287, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(519, 58);
            this.label1.TabIndex = 2;
            this.label1.Text = "Folha de Pagamento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Location = new System.Drawing.Point(13, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(198, 24);
            this.label4.TabIndex = 56;
            this.label4.Text = "Vale Alimentação";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.Location = new System.Drawing.Point(38, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 24);
            this.label5.TabIndex = 61;
            this.label5.Text = "Convênio";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(38, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 24);
            this.label6.TabIndex = 63;
            this.label6.Text = "FGTS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label7.Location = new System.Drawing.Point(38, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 24);
            this.label7.TabIndex = 65;
            this.label7.Text = "INSS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label8.Location = new System.Drawing.Point(3, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(131, 24);
            this.label8.TabIndex = 67;
            this.label8.Text = "Horas extras";
            // 
            // txtHoraExtra
            // 
            this.txtHoraExtra.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoraExtra.Location = new System.Drawing.Point(162, 67);
            this.txtHoraExtra.Multiline = true;
            this.txtHoraExtra.Name = "txtHoraExtra";
            this.txtHoraExtra.Size = new System.Drawing.Size(171, 20);
            this.txtHoraExtra.TabIndex = 68;
            // 
            // rbnNaoHEFerias
            // 
            this.rbnNaoHEFerias.AutoSize = true;
            this.rbnNaoHEFerias.BackColor = System.Drawing.Color.Transparent;
            this.rbnNaoHEFerias.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnNaoHEFerias.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.rbnNaoHEFerias.Location = new System.Drawing.Point(339, 69);
            this.rbnNaoHEFerias.Name = "rbnNaoHEFerias";
            this.rbnNaoHEFerias.Size = new System.Drawing.Size(101, 21);
            this.rbnNaoHEFerias.TabIndex = 69;
            this.rbnNaoHEFerias.TabStop = true;
            this.rbnNaoHEFerias.Text = "Não faz H.E";
            this.rbnNaoHEFerias.UseVisualStyleBackColor = false;
            // 
            // rbnSimHEFerias
            // 
            this.rbnSimHEFerias.AutoSize = true;
            this.rbnSimHEFerias.BackColor = System.Drawing.Color.Transparent;
            this.rbnSimHEFerias.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnSimHEFerias.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.rbnSimHEFerias.Location = new System.Drawing.Point(439, 68);
            this.rbnSimHEFerias.Name = "rbnSimHEFerias";
            this.rbnSimHEFerias.Size = new System.Drawing.Size(69, 21);
            this.rbnSimHEFerias.TabIndex = 70;
            this.rbnSimHEFerias.TabStop = true;
            this.rbnSimHEFerias.Text = "faz H.E";
            this.rbnSimHEFerias.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label11.Location = new System.Drawing.Point(24, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(155, 24);
            this.label11.TabIndex = 72;
            this.label11.Text = "Salário Líquido";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label13.Location = new System.Drawing.Point(38, 98);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 24);
            this.label13.TabIndex = 75;
            this.label13.Text = "IRRF";
            // 
            // txtFGTS
            // 
            this.txtFGTS.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFGTS.Location = new System.Drawing.Point(105, 25);
            this.txtFGTS.Mask = "%";
            this.txtFGTS.Name = "txtFGTS";
            this.txtFGTS.Size = new System.Drawing.Size(94, 23);
            this.txtFGTS.TabIndex = 81;
            // 
            // txtINSS
            // 
            this.txtINSS.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtINSS.Location = new System.Drawing.Point(105, 64);
            this.txtINSS.Mask = "%";
            this.txtINSS.Name = "txtINSS";
            this.txtINSS.Size = new System.Drawing.Size(94, 23);
            this.txtINSS.TabIndex = 82;
            // 
            // txtSalarioBruto
            // 
            this.txtSalarioBruto.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalarioBruto.Location = new System.Drawing.Point(185, 26);
            this.txtSalarioBruto.Mask = "$";
            this.txtSalarioBruto.Name = "txtSalarioBruto";
            this.txtSalarioBruto.Size = new System.Drawing.Size(140, 23);
            this.txtSalarioBruto.TabIndex = 84;
            // 
            // txtSalarioLiquido
            // 
            this.txtSalarioLiquido.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalarioLiquido.Location = new System.Drawing.Point(185, 95);
            this.txtSalarioLiquido.Mask = "$";
            this.txtSalarioLiquido.Name = "txtSalarioLiquido";
            this.txtSalarioLiquido.Size = new System.Drawing.Size(140, 23);
            this.txtSalarioLiquido.TabIndex = 85;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button3.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.Location = new System.Drawing.Point(188, 805);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(126, 42);
            this.button3.TabIndex = 86;
            this.button3.Text = "Voltar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.BackColor = System.Drawing.Color.Transparent;
            this.radioButton7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton7.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.radioButton7.Location = new System.Drawing.Point(509, 121);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(96, 21);
            this.radioButton7.TabIndex = 92;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "Possui vale";
            this.radioButton7.UseVisualStyleBackColor = false;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.BackColor = System.Drawing.Color.Transparent;
            this.radioButton8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton8.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.radioButton8.Location = new System.Drawing.Point(406, 121);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(97, 21);
            this.radioButton8.TabIndex = 91;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "Não possui";
            this.radioButton8.UseVisualStyleBackColor = false;
            // 
            // txtValeAlimentacao
            // 
            this.txtValeAlimentacao.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValeAlimentacao.Location = new System.Drawing.Point(217, 119);
            this.txtValeAlimentacao.Mask = "$";
            this.txtValeAlimentacao.Name = "txtValeAlimentacao";
            this.txtValeAlimentacao.Size = new System.Drawing.Size(169, 23);
            this.txtValeAlimentacao.TabIndex = 95;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(335, 90);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 34);
            this.button1.TabIndex = 96;
            this.button1.Text = "Calcular";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(18, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 24);
            this.label3.TabIndex = 54;
            this.label3.Text = "Vale Refeição";
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.BackColor = System.Drawing.Color.Transparent;
            this.radioButton6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton6.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.radioButton6.Location = new System.Drawing.Point(403, 81);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(97, 21);
            this.radioButton6.TabIndex = 89;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Não possui";
            this.radioButton6.UseVisualStyleBackColor = false;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.BackColor = System.Drawing.Color.Transparent;
            this.radioButton5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton5.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton5.Location = new System.Drawing.Point(506, 80);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(96, 21);
            this.radioButton5.TabIndex = 90;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Possui vale";
            this.radioButton5.UseVisualStyleBackColor = false;
            // 
            // txtValeRefeição
            // 
            this.txtValeRefeição.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValeRefeição.Location = new System.Drawing.Point(217, 79);
            this.txtValeRefeição.Mask = "$";
            this.txtValeRefeição.Name = "txtValeRefeição";
            this.txtValeRefeição.Size = new System.Drawing.Size(169, 23);
            this.txtValeRefeição.TabIndex = 94;
            // 
            // txtValeTransporte
            // 
            this.txtValeTransporte.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValeTransporte.Location = new System.Drawing.Point(217, 39);
            this.txtValeTransporte.Mask = "$";
            this.txtValeTransporte.Name = "txtValeTransporte";
            this.txtValeTransporte.Size = new System.Drawing.Size(169, 23);
            this.txtValeTransporte.TabIndex = 93;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.BackColor = System.Drawing.Color.Transparent;
            this.radioButton3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton3.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.radioButton3.Location = new System.Drawing.Point(506, 40);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(96, 21);
            this.radioButton3.TabIndex = 88;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Possui vale";
            this.radioButton3.UseVisualStyleBackColor = false;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.BackColor = System.Drawing.Color.Transparent;
            this.radioButton4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.radioButton4.Location = new System.Drawing.Point(403, 40);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(97, 21);
            this.radioButton4.TabIndex = 87;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Não possui";
            this.radioButton4.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(17, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 24);
            this.label2.TabIndex = 52;
            this.label2.Text = "Vale transporte";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.txtValeAlimentacao);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton7);
            this.groupBox1.Controls.Add(this.txtValeRefeição);
            this.groupBox1.Controls.Add(this.radioButton8);
            this.groupBox1.Controls.Add(this.txtValeTransporte);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.radioButton6);
            this.groupBox1.Controls.Add(this.radioButton5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(576, 364);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(623, 165);
            this.groupBox1.TabIndex = 97;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vales";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtSalarioFamilia);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtSalarioLiquido);
            this.groupBox2.Controls.Add(this.txtSalarioBruto);
            this.groupBox2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(576, 558);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(434, 143);
            this.groupBox2.TabIndex = 98;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Salários";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label19.Location = new System.Drawing.Point(23, 59);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(152, 24);
            this.label19.TabIndex = 97;
            this.label19.Text = "Salário Familia";
            // 
            // txtSalarioFamilia
            // 
            this.txtSalarioFamilia.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalarioFamilia.Location = new System.Drawing.Point(185, 60);
            this.txtSalarioFamilia.Mask = "$";
            this.txtSalarioFamilia.Name = "txtSalarioFamilia";
            this.txtSalarioFamilia.Size = new System.Drawing.Size(140, 23);
            this.txtSalarioFamilia.TabIndex = 98;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label10.Location = new System.Drawing.Point(41, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 24);
            this.label10.TabIndex = 71;
            this.label10.Text = "Salário Bruto";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button4.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button4.Location = new System.Drawing.Point(750, 805);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(121, 42);
            this.button4.TabIndex = 100;
            this.button4.Text = "Cadastrar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label15.Location = new System.Drawing.Point(6, 33);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 24);
            this.label15.TabIndex = 101;
            this.label15.Text = "Funcionário";
            // 
            // cboFunc
            // 
            this.cboFunc.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFunc.FormattingEnabled = true;
            this.cboFunc.Location = new System.Drawing.Point(166, 32);
            this.cboFunc.Name = "cboFunc";
            this.cboFunc.Size = new System.Drawing.Size(171, 25);
            this.cboFunc.TabIndex = 102;
            // 
            // txtIRRF
            // 
            this.txtIRRF.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIRRF.Location = new System.Drawing.Point(105, 100);
            this.txtIRRF.Mask = "%";
            this.txtIRRF.Name = "txtIRRF";
            this.txtIRRF.Size = new System.Drawing.Size(94, 23);
            this.txtIRRF.TabIndex = 103;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label16.Location = new System.Drawing.Point(38, 136);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 24);
            this.label16.TabIndex = 104;
            this.label16.Text = "DSR";
            // 
            // txtDSR
            // 
            this.txtDSR.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDSR.Location = new System.Drawing.Point(105, 138);
            this.txtDSR.Mask = "%";
            this.txtDSR.Name = "txtDSR";
            this.txtDSR.Size = new System.Drawing.Size(94, 23);
            this.txtDSR.TabIndex = 105;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label17.Location = new System.Drawing.Point(36, 108);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 24);
            this.label17.TabIndex = 106;
            this.label17.Text = "Atrasos";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label18.Location = new System.Drawing.Point(181, 108);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 24);
            this.label18.TabIndex = 108;
            this.label18.Text = "Faltas";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1220, 50);
            this.panel1.TabIndex = 114;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1137, 13);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(23, 22);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1177, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // nudAtrasos
            // 
            this.nudAtrasos.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudAtrasos.Location = new System.Drawing.Point(125, 111);
            this.nudAtrasos.Name = "nudAtrasos";
            this.nudAtrasos.Size = new System.Drawing.Size(35, 23);
            this.nudAtrasos.TabIndex = 115;
            // 
            // nudFaltas
            // 
            this.nudFaltas.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudFaltas.Location = new System.Drawing.Point(270, 109);
            this.nudFaltas.Name = "nudFaltas";
            this.nudFaltas.Size = new System.Drawing.Size(35, 23);
            this.nudFaltas.TabIndex = 116;
            // 
            // rbnSimHE
            // 
            this.rbnSimHE.AutoSize = true;
            this.rbnSimHE.BackColor = System.Drawing.Color.Transparent;
            this.rbnSimHE.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnSimHE.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.rbnSimHE.Location = new System.Drawing.Point(439, 32);
            this.rbnSimHE.Name = "rbnSimHE";
            this.rbnSimHE.Size = new System.Drawing.Size(69, 21);
            this.rbnSimHE.TabIndex = 120;
            this.rbnSimHE.TabStop = true;
            this.rbnSimHE.Text = "faz H.E";
            this.rbnSimHE.UseVisualStyleBackColor = false;
            // 
            // rbnNaoHE
            // 
            this.rbnNaoHE.AutoSize = true;
            this.rbnNaoHE.BackColor = System.Drawing.Color.Transparent;
            this.rbnNaoHE.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnNaoHE.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.rbnNaoHE.Location = new System.Drawing.Point(339, 33);
            this.rbnNaoHE.Name = "rbnNaoHE";
            this.rbnNaoHE.Size = new System.Drawing.Size(101, 21);
            this.rbnNaoHE.TabIndex = 119;
            this.rbnNaoHE.TabStop = true;
            this.rbnNaoHE.Text = "Não faz H.E";
            this.rbnNaoHE.UseVisualStyleBackColor = false;
            // 
            // txtHoraExtraFeriado
            // 
            this.txtHoraExtraFeriado.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoraExtraFeriado.Location = new System.Drawing.Point(230, 31);
            this.txtHoraExtraFeriado.Multiline = true;
            this.txtHoraExtraFeriado.Name = "txtHoraExtraFeriado";
            this.txtHoraExtraFeriado.Size = new System.Drawing.Size(103, 20);
            this.txtHoraExtraFeriado.TabIndex = 118;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label22.Location = new System.Drawing.Point(3, 28);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(221, 24);
            this.label22.TabIndex = 117;
            this.label22.Text = "Horas extras(Feriado)";
            // 
            // txtConvênio
            // 
            this.txtConvênio.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConvênio.Location = new System.Drawing.Point(155, 173);
            this.txtConvênio.Mask = "$";
            this.txtConvênio.Name = "txtConvênio";
            this.txtConvênio.Size = new System.Drawing.Size(94, 23);
            this.txtConvênio.TabIndex = 121;
            this.txtConvênio.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.lblResultadoHoras);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.rbnSimHE);
            this.groupBox3.Controls.Add(this.txtHoraExtra);
            this.groupBox3.Controls.Add(this.rbnNaoHE);
            this.groupBox3.Controls.Add(this.rbnNaoHEFerias);
            this.groupBox3.Controls.Add(this.txtHoraExtraFeriado);
            this.groupBox3.Controls.Add(this.rbnSimHEFerias);
            this.groupBox3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 557);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(519, 144);
            this.groupBox3.TabIndex = 122;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Hora Extra";
            // 
            // lblResultadoHoras
            // 
            this.lblResultadoHoras.AutoSize = true;
            this.lblResultadoHoras.BackColor = System.Drawing.Color.Transparent;
            this.lblResultadoHoras.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultadoHoras.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblResultadoHoras.Location = new System.Drawing.Point(330, 108);
            this.lblResultadoHoras.Name = "lblResultadoHoras";
            this.lblResultadoHoras.Size = new System.Drawing.Size(27, 24);
            this.lblResultadoHoras.TabIndex = 122;
            this.lblResultadoHoras.Text = ":0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label23.Location = new System.Drawing.Point(99, 108);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(233, 24);
            this.label23.TabIndex = 121;
            this.label23.Text = "Total ganho por horas";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.lblDSR);
            this.groupBox4.Controls.Add(this.lblIRRF);
            this.groupBox4.Controls.Add(this.txtConvênio);
            this.groupBox4.Controls.Add(this.lblINSS);
            this.groupBox4.Controls.Add(this.lblFGTS);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.txtFGTS);
            this.groupBox4.Controls.Add(this.txtINSS);
            this.groupBox4.Controls.Add(this.txtIRRF);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.txtDSR);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(244, 341);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(288, 210);
            this.groupBox4.TabIndex = 99;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Impostos e Benefícios";
            // 
            // lblDSR
            // 
            this.lblDSR.AutoSize = true;
            this.lblDSR.BackColor = System.Drawing.Color.Transparent;
            this.lblDSR.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDSR.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblDSR.Location = new System.Drawing.Point(244, 137);
            this.lblDSR.Name = "lblDSR";
            this.lblDSR.Size = new System.Drawing.Size(22, 24);
            this.lblDSR.TabIndex = 113;
            this.lblDSR.Text = "0";
            // 
            // lblIRRF
            // 
            this.lblIRRF.AutoSize = true;
            this.lblIRRF.BackColor = System.Drawing.Color.Transparent;
            this.lblIRRF.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIRRF.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblIRRF.Location = new System.Drawing.Point(244, 99);
            this.lblIRRF.Name = "lblIRRF";
            this.lblIRRF.Size = new System.Drawing.Size(22, 24);
            this.lblIRRF.TabIndex = 112;
            this.lblIRRF.Text = "0";
            // 
            // lblINSS
            // 
            this.lblINSS.AutoSize = true;
            this.lblINSS.BackColor = System.Drawing.Color.Transparent;
            this.lblINSS.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblINSS.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblINSS.Location = new System.Drawing.Point(244, 64);
            this.lblINSS.Name = "lblINSS";
            this.lblINSS.Size = new System.Drawing.Size(22, 24);
            this.lblINSS.TabIndex = 111;
            this.lblINSS.Text = "0";
            // 
            // lblFGTS
            // 
            this.lblFGTS.AutoSize = true;
            this.lblFGTS.BackColor = System.Drawing.Color.Transparent;
            this.lblFGTS.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFGTS.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblFGTS.Location = new System.Drawing.Point(243, 25);
            this.lblFGTS.Name = "lblFGTS";
            this.lblFGTS.Size = new System.Drawing.Size(22, 24);
            this.lblFGTS.TabIndex = 110;
            this.lblFGTS.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label28.Location = new System.Drawing.Point(204, 136);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(40, 24);
            this.label28.TabIndex = 109;
            this.label28.Text = "R$:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label27.Location = new System.Drawing.Point(205, 96);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(40, 24);
            this.label27.TabIndex = 108;
            this.label27.Text = "R$:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label26.Location = new System.Drawing.Point(204, 63);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(40, 24);
            this.label26.TabIndex = 107;
            this.label26.Text = "R$:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label25.Location = new System.Drawing.Point(204, 25);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(40, 24);
            this.label25.TabIndex = 106;
            this.label25.Text = "R$:";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.lblResultado);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.txtSalário);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.cboFunc);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.nudFaltas);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.nudAtrasos);
            this.groupBox5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(382, 138);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(360, 194);
            this.groupBox5.TabIndex = 122;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Funcionário";
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.BackColor = System.Drawing.Color.Transparent;
            this.lblResultado.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultado.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblResultado.Location = new System.Drawing.Point(238, 151);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(22, 24);
            this.lblResultado.TabIndex = 119;
            this.lblResultado.Text = "0";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label35.Location = new System.Drawing.Point(198, 150);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(40, 24);
            this.label35.TabIndex = 118;
            this.label35.Text = "R$:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label9.Location = new System.Drawing.Point(56, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 24);
            this.label9.TabIndex = 50;
            this.label9.Text = "Salário";
            // 
            // txtSalário
            // 
            this.txtSalário.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalário.Location = new System.Drawing.Point(166, 69);
            this.txtSalário.Mask = "$";
            this.txtSalário.Name = "txtSalário";
            this.txtSalário.Size = new System.Drawing.Size(140, 23);
            this.txtSalário.TabIndex = 83;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label33.Location = new System.Drawing.Point(101, 150);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(100, 24);
            this.label33.TabIndex = 117;
            this.label33.Text = "Perdido -";
            // 
            // dtpDisponivel
            // 
            this.dtpDisponivel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDisponivel.Location = new System.Drawing.Point(298, 748);
            this.dtpDisponivel.Name = "dtpDisponivel";
            this.dtpDisponivel.Size = new System.Drawing.Size(234, 23);
            this.dtpDisponivel.TabIndex = 126;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label21.Location = new System.Drawing.Point(297, 708);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(214, 24);
            this.label21.TabIndex = 125;
            this.label21.Text = "Folha disponivel em";
            // 
            // cboFormaPagamento
            // 
            this.cboFormaPagamento.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFormaPagamento.FormattingEnabled = true;
            this.cboFormaPagamento.Items.AddRange(new object[] {
            "Crédito",
            "Débito ",
            "Cheque",
            "Dinheiro"});
            this.cboFormaPagamento.Location = new System.Drawing.Point(576, 746);
            this.cboFormaPagamento.Name = "cboFormaPagamento";
            this.cboFormaPagamento.Size = new System.Drawing.Size(213, 25);
            this.cboFormaPagamento.TabIndex = 124;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label20.Location = new System.Drawing.Point(572, 708);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(238, 24);
            this.label20.TabIndex = 123;
            this.label20.Text = "Forma de Pagamento";
            // 
            // frmCadastroFolhadePagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1217, 859);
            this.Controls.Add(this.dtpDisponivel);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.cboFormaPagamento);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCadastroFolhadePagamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Folha_de_Pagamento";
            this.Load += new System.EventHandler(this.Folha_de_Pagamento_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAtrasos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltas)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtHoraExtra;
        private System.Windows.Forms.RadioButton rbnNaoHEFerias;
        private System.Windows.Forms.RadioButton rbnSimHEFerias;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox txtFGTS;
        private System.Windows.Forms.MaskedTextBox txtINSS;
        private System.Windows.Forms.MaskedTextBox txtSalarioBruto;
        private System.Windows.Forms.MaskedTextBox txtSalarioLiquido;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.MaskedTextBox txtValeAlimentacao;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.MaskedTextBox txtValeRefeição;
        private System.Windows.Forms.MaskedTextBox txtValeTransporte;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboFunc;
        private System.Windows.Forms.MaskedTextBox txtIRRF;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox txtDSR;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.MaskedTextBox txtSalarioFamilia;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.NumericUpDown nudAtrasos;
        private System.Windows.Forms.NumericUpDown nudFaltas;
        private System.Windows.Forms.RadioButton rbnSimHE;
        private System.Windows.Forms.RadioButton rbnNaoHE;
        private System.Windows.Forms.TextBox txtHoraExtraFeriado;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox txtConvênio;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblResultadoHoras;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblDSR;
        private System.Windows.Forms.Label lblIRRF;
        private System.Windows.Forms.Label lblINSS;
        private System.Windows.Forms.Label lblFGTS;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox txtSalário;
        private System.Windows.Forms.DateTimePicker dtpDisponivel;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cboFormaPagamento;
        private System.Windows.Forms.Label label20;
    }
}