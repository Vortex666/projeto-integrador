﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM
{
    public partial class frmCadastroFolhadePagamento : Form
    {
        public frmCadastroFolhadePagamento()
        {
            InitializeComponent();
        }

        private void Folha_de_Pagamento_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Application.Exit();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Horas Trabalhadas

            decimal salarioNominal = Convert.ToDecimal(txtSalário.Text);

            int horasTrabalhadasdia = 8 * 5;
            int horasTrabalhadasMes = horasTrabalhadasdia * 5;

            decimal valorHoraTrabalhada = salarioNominal / horasTrabalhadasMes;

            //Atrasos 
            decimal horasAtraso = valorHoraTrabalhada * nudAtrasos.Value;

            //Faltas

            decimal faltas = salarioNominal / 30;
            decimal valorFaltas = faltas * nudFaltas.Value;

            //Valor Hora Extra Dia
            if (rbnNaoHE.Checked == true)
            {
                txtHoraExtra.Text = "0";
                
            }
            
           
                decimal porcentagemHoraExtra = (valorHoraTrabalhada * 50) / 100;
                decimal valorHoraExtra = valorHoraTrabalhada + porcentagemHoraExtra;
                decimal horasExtras = valorHoraExtra * Convert.ToInt32(txtHoraExtra.Text);

                if (horasExtras >= 1)
                {
                    decimal horasExtrasdia = horasExtras;

                }
                else
                {
                    lblResultadoHoras.Text = "0,00";
                }
            

            //Horas extras no feriado

            decimal porcentagemHoraExtraferiado = (valorHoraTrabalhada * 100) / 100;
            decimal valorHoraExtraferiado = valorHoraTrabalhada + porcentagemHoraExtraferiado;
            decimal horasExtrasFeriado = valorHoraExtraferiado * Convert.ToInt32(txtHoraExtraFeriado.Text);


            if (horasExtrasFeriado >= 1)
            {
                decimal horasExtrasdomingo = horasExtrasFeriado;

            }
         
            



            decimal resultado = horasExtras + horasExtrasFeriado;
            resultado = Math.Round(resultado, 2);
            txtHoraExtra.Text = Convert.ToString(resultado);


            //Salario Base do INSS
            decimal salarioBruto = salarioNominal + horasExtras - horasAtraso - valorFaltas;
            decimal salarioBaseINSS = salarioBruto;

            if (salarioBaseINSS <= 1659.38m)
            {
                decimal salario = (salarioBaseINSS * 8) / 100;
                salario = Math.Round(salario, 2);
               txtINSS.Text = Convert.ToString(salario);
            }
            else if (salarioBaseINSS >= 1659.39m && salarioBaseINSS <= 2765.66m)
            {
                decimal salario = (salarioBaseINSS * 9) / 100;
                salario = Math.Round(salario, 2);
                txtINSS.Text = Convert.ToString(salario);

            }
            else if (salarioBaseINSS >= 2765.67m)
            {
                decimal salario = (salarioBaseINSS * 11) / 100;
                salario = Math.Round(salario, 2);
                txtINSS.Text = Convert.ToString(salario);
            }

            //Imposto de Renda 
            decimal baseCalculoIR = salarioBaseINSS - Convert.ToDecimal(txtIRRF.Text);

            if (baseCalculoIR <= 1903.98m)
            {
                txtIRRF.Text = "0,00";

            }
            else if (baseCalculoIR >= 1903.99m && baseCalculoIR <= 2826.65m)
            {
                decimal imposto = (baseCalculoIR * 7.5m) / 100;
                decimal reduzir = 142.80m;
                reduzir = Math.Round(reduzir, 2);
                txtIRRF.Text = Convert.ToString(reduzir);

            }
            else if (baseCalculoIR >= 2826.66m && baseCalculoIR <= 3751.05m)
            {
                decimal imposto = (baseCalculoIR * 15) / 100;
                decimal reduzir = 354.80m;
                reduzir = Math.Round(reduzir, 2);
                txtIRRF.Text = Convert.ToString(reduzir);

            }
            else if (baseCalculoIR >= 3751.06m && baseCalculoIR <= 4664.68m)
            {
                decimal imposto = (baseCalculoIR * 22.5m) / 100;
                decimal reduzir = 636.13m;
                reduzir = Math.Round(reduzir, 2);
                txtIRRF.Text = Convert.ToString(reduzir);
            }
            else if (baseCalculoIR >= 4664.88m)
            {
                decimal imposto = (baseCalculoIR * 27.5m) / 100;
                decimal reduzir = 869.36m;
                reduzir = Math.Round(reduzir, 2);
                txtIRRF.Text = Convert.ToString(reduzir);
            }

            //FGTS
            decimal fgts = (salarioBaseINSS * 8) / 100;
            fgts = Math.Round(fgts, 2);
            txtFGTS.Text = Convert.ToString(fgts);


            //Salario Liquido
            decimal salarioliquido = salarioNominal + Convert.ToDecimal(txtHoraExtra.Text) - horasAtraso - valorFaltas - Convert.ToDecimal(txtINSS.Text)
                                                    - Convert.ToDecimal(txtIRRF.Text) - Convert.ToDecimal(txtValeTransporte.Text) - Convert.ToDecimal(txtFGTS.Text);
            salarioliquido = Math.Round(salarioliquido, 2);
            txtSalarioLiquido.Text = Convert.ToString(salarioliquido);
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            FolhaPagamentoDTO folhaPagamento = new FolhaPagamentoDTO();
            folhaPagamento.SalárioLíquido = Convert.ToDecimal(txtSalarioLiquido.Text);
            folhaPagamento.SalárioFamilia = Convert.ToDouble(txtSalarioFamilia.Text);
            folhaPagamento.SalárioBruto = Convert.ToDecimal(txtSalarioBruto.Text);
            folhaPagamento.ValeAlimentação = Convert.ToDecimal(txtValeAlimentacao.Text);
            folhaPagamento.ValeRefeição = Convert.ToDecimal(txtValeRefeição.Text);
            folhaPagamento.ValeTransporte = Convert.ToDecimal(txtValeTransporte.Text);
            FuncionarioDTO func = cboFunc.SelectedItem as FuncionarioDTO;
            folhaPagamento.Salário = Convert.ToDecimal(txtSalário.Text);
            folhaPagamento.Atrasos = Convert.ToInt32(nudAtrasos.Value);
            folhaPagamento.Faltas = Convert.ToInt32(nudFaltas.Value);
            folhaPagamento.HoraExtra = Convert.ToInt32(txtHoraExtra.Text);
            folhaPagamento.Disponível = dtpDisponivel.Value;
            folhaPagamento.FormaPagamento = cboFormaPagamento.SelectedText;
            folhaPagamento.Fgts = Convert.ToDecimal(lblFGTS.Text);
            folhaPagamento.Inss = Convert.ToDecimal(lblINSS.Text);
            folhaPagamento.Irrf = Convert.ToDecimal(lblIRRF.Text);
            folhaPagamento.DSR = Convert.ToDecimal(lblDSR.Text);
            folhaPagamento.Convênio = Convert.ToDecimal(txtConvênio.Text);


        }

        void CarregarCombos()
        {
            FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness();
            List<FuncionarioDTO> funcionarios = funcionarioBusiness.Listar();

            cboFunc.ValueMember = nameof(FuncionarioDTO.Id);
            cboFunc.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFunc.DataSource = funcionarios;
        }
    }
}
