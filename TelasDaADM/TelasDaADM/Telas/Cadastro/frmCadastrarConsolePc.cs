﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.Telas.Cadastro
{
    public partial class frmCadastrarConsolePc : Form
    {
        public frmCadastrarConsolePc()
        {
            InitializeComponent();
        }
        BindingList<string> pecasAdicionais = new BindingList<string>();


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            ConsoleComputadorDTO consoleComputador = new ConsoleComputadorDTO();
            consoleComputador.Nome = txtNome.Text;
            consoleComputador.Marca = txtMarca.Text;
            consoleComputador.PrecoPcConsole = Convert.ToDecimal(txtPrecoConsolePc.Text);
            consoleComputador.Quantidade = Convert.ToInt32(nudQtdConsolePc.Value);
            ConsoleComputadorBusiness business = new ConsoleComputadorBusiness();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pecas = txtPecas.Text;
            int qtd = Convert.ToInt32(nudQtdPecas.Value);
            for (int i = 0; i < qtd; i++)
            {
                pecasAdicionais.Add(pecas);
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Application.Exit();
        }
    }
}
