﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM
{
    public partial class frmCadastroFornecedor : Form
    {
        public frmCadastroFornecedor()
        {
            InitializeComponent();
            CarregarCombos();
        }

        BindingList<ProdutoDTO> produtos = new BindingList<ProdutoDTO>();

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Application.Exit();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }


        private void button2_Click(object sender, EventArgs e)
        {
            ProdutoDTO produto = cboProduto.SelectedItem as ProdutoDTO;
            ConsoleComputadorDTO conspc = cboConsPC.SelectedItem as ConsoleComputadorDTO;
            FliperamaDTO flip = cboFlip.SelectedItem as FliperamaDTO;

            EnderecoDTO endereco = new EnderecoDTO();
            endereco.CEP = txtCEP.Text;
            endereco.Cidade = txtCidade.Text;
            endereco.Complemento = txtComplemento.Text;
            endereco.Bairro = txtBairro.Text;
            EnderecoBusiness business = new EnderecoBusiness();
            endereco.Id = business.Salvar(endereco);

            FornecedorDTO fornec = new FornecedorDTO();
            fornec.Nome = txtNome.Text;
            fornec.CPFCNPJ = txtCEP.Text;
            fornec.DataNascimento = dtpNascimento.Value;
            fornec.Email = txtEmail.Text;
            fornec.Telefone = txtTelefone.Text;
            fornec.IdEndereco = endereco.Id;
            fornec.IdProduto = produto.Id;
            fornec.IdFliperama = flip.Id;
            fornec.IdConsoleComputador = conspc.Id;
            FornecedorBusiness busines = new FornecedorBusiness();
            fornec.Id = busines.Salvar(fornec);

            MessageBox.Show("Fornecedor Cadastrado com sucesso", "Flip's House" , MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        void CarregarCombos()
        {
            ProdutoBusiness produtoBusiness = new ProdutoBusiness();
            List<ProdutoDTO> produtoListar = produtoBusiness.Listar();
            produtos = new BindingList<ProdutoDTO>(produtoListar);

            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = produtos;

            FliperamaBusiness busines = new FliperamaBusiness();
            List<FliperamaDTO> listas = busines.Listar();

            cboFlip.ValueMember = nameof(FliperamaDTO.Id);
            cboFlip.DisplayMember = nameof(FliperamaDTO.Nome);
            cboFlip.DataSource = listas;

            ConsoleComputadorBusiness butiness = new ConsoleComputadorBusiness();
            List<ConsoleComputadorDTO> listinhas = butiness.Listar();
            
            
        }
        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
