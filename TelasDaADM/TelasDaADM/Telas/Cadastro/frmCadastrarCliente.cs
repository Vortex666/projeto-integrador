﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM
{
    public partial class frmCadastrarCliente : Form
    {
        public frmCadastrarCliente()
        {
            InitializeComponent();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                EnderecoDTO endereco = new EnderecoDTO();
                endereco.CEP = txtCEP.Text;
                endereco.Complemento = txtComplemento.Text;
                endereco.Cidade = txtCidade.Text;
                endereco.Bairro = txtBairro.Text;


                EnderecoBusiness busines = new EnderecoBusiness();
                endereco.Id = busines.Salvar(endereco);

            

                ClienteDTO cliente = new ClienteDTO();

                cliente.Nome = txtNome.Text;
                cliente.DataNascimento = dtpNascimento.Value;
                cliente.Celular = txtTelefone.Text;
                cliente.Email = txtEmail.Text;
                cliente.CPF = txtCPF.Text;
                cliente.RG = txtRG.Text;
                cliente.Telefone = txtTelefone.Text;
                cliente.IdEndereco = endereco.Id;
                ClienteBusiness business = new ClienteBusiness();
                cliente.Id = business.Salvar(cliente);


           

            


            MessageBox.Show("Cliente cadastrado com sucesso", "Flip's House", MessageBoxButtons.OK, MessageBoxIcon.Information);


            
            }
           catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Flip's House",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {

            
                MessageBox.Show("Deu algo de errado, por favor contatar o Administrador", "Flip's House",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }


            
            

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Application.Exit();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
