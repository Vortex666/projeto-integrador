﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.Cadastro;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;


namespace TelasDaADM.Consultar
{
    public partial class frmConsultarFuncionario : Form
    {
        public frmConsultarFuncionario()
        {
            InitializeComponent();
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            FuncionarioDTO func = dgvFuncionario.Rows[e.RowIndex].DataBoundItem as FuncionarioDTO;
              
        }

        

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Hide();

            Application.Exit();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        private void ptbAlterar_Click(object sender, EventArgs e)
        {

            frmCadastrarFuncionario tela = new frmCadastrarFuncionario();
            

        }
    }
}
