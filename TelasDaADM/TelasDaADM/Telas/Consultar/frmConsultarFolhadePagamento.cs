﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM.Consultar
{
    public partial class frmConsultarFolhadePagamento : Form
    {
        public frmConsultarFolhadePagamento()
        {
            InitializeComponent();
            List<FolhaPagamentoDTO> folha = new List<FolhaPagamentoDTO>();

            gvFolha.DataSource = folha;
        }

        private void frmConsultarFolhadePagamento_Load(object sender, EventArgs e)
        {

        }

        public List<FolhaPagamentoDTO> ListarFolha()
        {
            List<FolhaPagamentoDTO> folha;
            FolhaPagamentoBusiness business = new FolhaPagamentoBusiness();
            folha = business.Listar();
            return folha;

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Application.Exit();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }
    }
}
