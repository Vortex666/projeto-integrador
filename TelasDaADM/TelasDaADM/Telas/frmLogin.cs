﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelasDaADM.DB.Business;
using TelasDaADM.DB.DTOs;

namespace TelasDaADM
{
    public partial class frmLogin : Form
    {
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Flip's House",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        public frmLogin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO funcionario = business.Logar(txtUsuario.Text, txtSenha.Text);

                if (funcionario != null)
                {
                    UserSession.UsuarioLogado = funcionario;

                    frmMenu menu = new frmMenu();
                    menu.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "Flip's House", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possivel logar: " + ex.Message);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Application.Exit();
        }
    }
}
